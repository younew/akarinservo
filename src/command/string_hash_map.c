/* $$ LICENSE HEADER START $$ */
/*
 * Project CANOE
 * -------------
 * A simple, high performance, portable GUI framework.
 *
 * Copyright 2015 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $$ LICENSE HEADER END $$ */

#include "string_hash_map.h"
#include <stdio.h>

static inline unsigned int BKDRHash(char *str)
{
    unsigned int seed = 131;
    unsigned int hash = 0;
    
    while (*str)
    {
        hash = hash * seed + (*str++);
    }
    
    return (hash & 0x7FFFFFFF);
}

static inline
unsigned int getFinateHash(char *str, int max)
{
    return BKDRHash(str) % max;
}

StringHashMap *stringHashMapCreate(int hashArraySize)
{
    StringHashMap *map = malloc(sizeof(StringHashMap));
    if(!map) goto str_hash_map_err_out_of_memory;
    
    map->hashArraySize = hashArraySize;
    
    map->root = malloc(sizeof(StringHashMapNode) * map->hashArraySize);
    
    if(!map->root)
    {
        free(map);
        goto str_hash_map_err_out_of_memory;
    }
    
    for(int i = 0; i < map->hashArraySize; i++)
    {
        /* root array, dummy nodes */
        strcpy(map->root[i].key, "dummy");
        
        map->root[i].next = malloc(sizeof(StringHashMapNode));
        map->root[i].next->next = NULL;
        
        map->root[i].value = (void *)-1;
    }
    
    return map;
    
str_hash_map_err_out_of_memory:
    fprintf(stderr, "String hash map: error: out of memory\n");
    return NULL;
}

StringHashMapNode *stringHashMapFindImp(StringHashMap *map, char *key)
{
    int index = getFinateHash(key, map->hashArraySize);
    
    /* root level and nothing stored in this node */
    if(!map->root[index].next)
        return NULL;
    
    /* stop immediately if the first entry matches the key */
    if(strcmp(key, map->root[index].key) == 0)
        return &map->root[index];
    
    StringHashMapNode *current = map->root[index].next;
    StringHashMapNode *prev = &map->root[index];
    
    while(current && current->next)
    {
        prev = current;
        current = current->next;
        
        if(strcmp(key, prev->key) == 0)
            return prev;
    }
    
    assert(current);
    assert(!current->next);
    
    return prev;
}

StringHashMapNode *stringHashMapFind(StringHashMap *map, char *key)
{
    StringHashMapNode *result = stringHashMapFindImp(map, key);
    
    if(result && strcmp(result->key, key) == 0)
        return result;
    else
        return NULL;
}

StringHashMap *stringHashMapInsert(StringHashMap *map, char *key, void *value)
{
    StringHashMapNode *item = stringHashMapFindImp(map, key);
    
    if(item)
    {
        /* test if the key is the same */
        if(strcmp(key, item->key) == 0)
        {
        	printf("key duplicate: [%s] [%s]", key, item->key);
            assert(0); /* duplicate key */
        }
        
        item = item->next;
    }
    else
    {
        item = &map->root[getFinateHash(key, map->hashArraySize)];
    }
    
    strcpy(item->key, key);
    item->value = value;
    
    item->next = malloc(sizeof(StringHashMapNode));
    item->next->next = NULL;
    strcpy(item->next->key, "new");
    
    return map;
}

StringHashMapNode *stringHashMapRemove(StringHashMap *map, char *key)
{
    StringHashMapNode *item = stringHashMapFindImp(map, key);
    
    if(item)
    {
        int index = getFinateHash(key, map->hashArraySize);
        
        StringHashMapNode *current = &map->root[index];
        StringHashMapNode *prev = current;
        
        /* root level item */
        if(item == current)
        {
            return item;
        }
        
        while(current)
        {
            prev = current;
            current = current->next;
            if(current == item)
            {
                (prev)->next = item->next;
                return item;
            }
        }
    }
    
    return NULL;
}

void stringHashMapShowMap(StringHashMap *map)
{
    for(int i = 0; i < map->hashArraySize; i++)
    {
        StringHashMapNode *current = &map->root[i];
        
        if(current->next)
            printf("%p[%s, %d] -> ", current, current->key, (int)current->value);
        else
            printf("%p[(new - empty)] -> ", current);
        
        while (current)
        {
            current = current->next;
            if(current)
            {
                if(current->next)
                    printf("%p[%s, %d] -> ", current, current->key, (int)current->value);
                else
                    printf("%p[(new - empty)] -> ", current);
            }
            else
                printf("NULL");
        }
        
        printf("\n");
    }
}

void stringHashMapDestroy(StringHashMap *map)
{
    assert(map);
    
    for(int i = 0; i < map->hashArraySize; i++)
    {
        StringHashMapNode *item = map->root[i].next;
        
        while (item)
        {
            StringHashMapNode *next = item->next;
            
            free(item);
            
            item = next;
        }
    }
    
    free(map->root);
    free(map);
}
