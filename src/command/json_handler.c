/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "command.h"

#include "../hw/sharc_hw.h"
#include "../motor_control.h"
#include "../utils/cJSON.h"

#include <inttypes.h>
#include <stdint.h>

int json_execute_command(char *cmd, int argc, char *argv[])
{
	command_handler_func *handler = find_command(command_list, cmd);

	if(handler)
	{
	    return handler(&mc_session, argc, argv);
	}

	return -1;
}

int json_command_dummy(char *cmd, int sender, int recv, int argc, char *argv[])
{
    uprintf("{\"sender\":%d,\"recv\":%d,\"response\":\"",
    		mc_session.controller_configs.local_address, sender);

    int ret = json_execute_command(cmd, argc, argv);

    /* command not found */
    if(ret == -1)
    {
    	uputs("???");
    }

    uprintf("\",\"return\":%d}\r\n", ret);

    return ret;
}

int json_call_parse(char *json_msg)
{
    int sender, receiver;
    char func[32];

    cJSON *p, *pjson;

    int args_count = 0;

    int   argc = 0;
    char *argv[MAX_ARG_NUM];
    memset(argv, 0, MAX_ARG_NUM);

    if(!json_msg)
    {
        goto __parse_invalid;
    }

    pjson = cJSON_Parse(json_msg);
    if(!pjson)
    {
        goto __parse_invalid;
    }

    /* parse sender */
    p = cJSON_GetObjectItem(pjson, "sender");
    if(!p) goto __parse_error;
    sender = p->valueint;

    /* parse receiver */
    p = cJSON_GetObjectItem(pjson, "recv");
    if(!p) goto __parse_error;
    receiver = p->valueint;

    /* parse function to call */
    p = cJSON_GetObjectItem(pjson, "func");
    if(!p) goto __parse_error;
    strncpy(func, p->valuestring, 32);

    p = cJSON_GetObjectItem(pjson, "args");
    if(!p) goto __parse_error;

    args_count = cJSON_GetArraySize(p);
    for(int i = 0; i < args_count; i++)
    {
        cJSON * pSub = cJSON_GetArrayItem(p, i);
        argv[argc++] = pSub->valuestring;
    }

    //uprintf("sender: %d, recv: %d, func: %s\r\n", sender, receiver, func);

    if(receiver == mc_session.controller_configs.local_address || receiver == -2 || receiver == -1)
    {
    	if(receiver == -1)
    	{
    		uart_out_redirect(NULL);
    	}

    	json_command_dummy(func, sender, receiver, argc, argv);

    	if(receiver == -1)
      	{
    		uart_out_redirect(&uart_tx_fifo);
      	}
    }


__parse_done:
    cJSON_Delete(pjson);
    return 0;

__parse_error:
    cJSON_Delete(pjson);
    return 2;

__parse_invalid:
    return 1;
}

