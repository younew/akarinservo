/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include <stdint.h>

typedef float float32_t;

typedef struct
{
	float32_t p, i, d;
} pid_settings_t;

typedef struct
{
	uint32_t id;		/* encoder ID */
	uint32_t  poles;		/* number of poles of the motor */
	float32_t encoder_offset;

	int32_t direction;

	float32_t torque_limit_ccw, torque_limit_cw;
	float32_t rotor_vel_limit;
	float32_t rated_rpm;
	float32_t defulat_acc, max_acc;

	float vel_feedforward;
	float torque_feedforward;
	float torque_coeff;
	float motor_current_protect_limit;

	float inertia_rotor_calibrated;
	float inertia_rotor_cal_torque;
	float inertia_rotor_cal_duration;

	float cogging_torque[512];

} akarin_encoder_t;

#define WREN 0x06
#define WRDI 0x04
#define RDSR 0x05
#define WRSR 0x01
#define READ 0x03
#define WRITE 0x02

int mag_encoder_detect(void);

int encoder_eeprom_busy(void);

void encoder_eeprom_write_enable(void);

void encoder_eeprom_write_byte(int address, uint32_t byte);

uint32_t encoder_eeprom_read_byte(int address);

void encoder_eeprom_save(akarin_encoder_t *encoder, int offset);

akarin_encoder_t *encoder_eeprom_restore(akarin_encoder_t *encoder, int offset);

void mag_encoder_release(void);

uint32_t encoder_fram_read_byte(int address);

void encoder_eeprom_write_byte(int address, uint32_t byte);

int encoder_fram_write_word(int address, uint32_t word);

uint32_t encoder_fram_read_word(int address);

#endif /* ENCODER_H_ */
