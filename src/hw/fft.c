/*
 * Copyright (c) 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <builtins.h>

#include <platform_include.h>
#include <processor_include.h>
#include <services/int/adi_int.h>

#define PCI 		0x80000
#define COEFFSEL 	0x100000
#define OFFSET_MASK 0x7FFFF

volatile float inputdata[512];

static float coeffdata[512] = {
		#include "twiddle256complx.dat"
}; /*512 twiddle data in the form of real and imag. coeff are in terms of cos, -sin, sin, cos.*/

volatile float outputdata[512];

int dataTxTCB[6] = { 0, 0, 0, 512, 1, (int)inputdata  };
int coeffTCB[6]  = { 0, 0, 0, 512, 1, (int)coeffdata  };
int dataRxTCB[6] = { 0, 0, 0, 512, 1, (int)outputdata };

int fft_is_busy(void)
{
	return (*pFFTDMASTAT & ODMACHIRPT) == 0;
}

float *fft256_read(float *output)
{
	memcpy(output, (float *)outputdata, sizeof(float) * 512);
	return output;
}

int fft256_dma_start(float *input)
{
#define EFFECT_LATENCY_DWELL() NOP();NOP();NOP();NOP();NOP();

	memcpy((float *)inputdata, input, sizeof(float) * 512);
	unsigned int temp;

	/* Selecting FFT accelerator */
	*pPMCTL1 &= ~(BIT_17 | BIT_18);
	*pPMCTL1 |= FFTACCSEL;

	/* PMCTL1 effect latency */
	EFFECT_LATENCY_DWELL();

	/*Clearing the FFTCTL registers*/
	*pFFTCTL2 = 0;
	*pFFTCTL1 = FFT_RST;
	EFFECT_LATENCY_DWELL();

	temp= ((int)(dataTxTCB) + 5) & OFFSET_MASK;  /*Set up coeff TCB*/
	coeffTCB[0] = temp;
	*pCPIFFT= (((int)coeffTCB + 5) & OFFSET_MASK) | COEFFSEL;   /* Start transmit TCB.*/

	/* Enable receiveTCB   need not wait for the transmit TCB to complete. */
	/* Enables interrupt after DMA for receive*/
	temp = PCI;
	dataRxTCB[0] = temp;
	temp= (int)dataRxTCB + 5;

	*pCPOFFT = temp;   /* Starts DMA.*/
	*pFFTCTL2 = VDIMDIV16_16 | FFT_LOG2VDIM_8;  /* Set up VDIM values*/
	*pFFTCTL1 = FFT_EN | FFT_START | FFT_DMAEN;

	EFFECT_LATENCY_DWELL();

	return 0;
}
