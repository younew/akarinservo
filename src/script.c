/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "script.h"
#include "utils/log.h"
#include "utils/algorithms.h"

uint32_t scripts_storage[SCRIPT_STORAGE_LEN];
scripts_list_t script_list;

uint32_t script_malloc_count = 0;

static char *str_from_uint32_array(uint32_t *arr, char *str, int len)
{
    int count = 0;
    for(int i = 0; i < len; i++)
    {
        str[count++] = arr[i] & 0xff;
        str[count++] = (arr[i] >> 8) & 0xff;
        str[count++] = (arr[i] >> 16) & 0xff;
        str[count++] = (arr[i] >> 24) & 0xff;
    }

    return str;
}

static uint32_t *str_to_uint32_array(uint32_t *arr, char *str, int len)
{
    int count = 0;
    for(int i = 0; i < len; i++)
    {
        arr[count >> 2] |= str[i] << (((count % 4)) * 8);
        count++;
    }

    return arr;
}

script_t *script_new(char *name, char *text)
{
    script_t *script = script_malloc(sizeof(script_t));
    script->name = script_malloc(sizeof(char) * (strlen(name) + 1));
    script->text = script_malloc(sizeof(char) * (strlen(text) + 1));

    script->name_len = (uint32_t)strlen(name);
    script->text_len = (uint32_t)strlen(text);

    strcpy(script->name, name);
    strcpy(script->text, text);

    return script;
}

script_t *script_delete(scripts_list_t *list, char *name)
{
	/* search the script */
	script_t *prev;
	script_t *s = script_find(list, name, &prev);

	/* stop if it's not found */
	if(!s)
	{
		return NULL;
	}

	/* the script is the first one in the linked list */
	if(s == list->script)
	{
		list->script = list->script->next;
	}
	else
	{
		prev->next = s->next;
	}

	return s;
}

script_t *script_add(scripts_list_t *list, char *name, char *text)
{
    script_t *current_script = NULL;

    /* script list is empty */
    if(!list->script)
    {
        list->script = script_new(name, text);
        list->script->next = NULL;

        current_script = list->script;
    }
    else
    {
        /* script list not empty */
        current_script = list->script;
        while(current_script->next)
        {
            current_script = current_script->next;
        }

        current_script->next = script_new(name, text);
        current_script->next->next = NULL;
    }

    return current_script;
}

scripts_list_t *script_save(scripts_list_t *list, uint32_t *storage)
{
    script_t *current = list->script;
    uint32_t *p = storage;
    uint32_t *p_head = p;

    while(current)
    {
    	p_head = p;

    	/* save length of name */
        *p++ = (uint32_t)current->name_len;
        str_to_uint32_array(p, current->name, current->name_len);
        p += ((current->name_len - 1) >> 2) + 1;

        /* save text */
        *p++ = (uint32_t)current->text_len;
        str_to_uint32_array(p, current->text, current->text_len);

        if(current->text_len == 0)
        {
        	*p = '\0';
        	p++;
        }
        else
        {
        	p += ((current->text_len - 1) >> 2) + 1;
        }
        //printf("len: %d\n", current->text_len);
        //printf("offset: %d\n", p - p_head);

        if(p - p_head <= 0)
        {
        	printf("len <=0???\n");
        	continue;
        }

        /* calculate checksum */
        uint32_t crc = crc32(0x93, p_head, p - p_head);
        /* save checksum */
        *p++ = crc;

        current = current->next;
    }

    return list;
}

scripts_list_t *script_load(scripts_list_t *list, uint32_t *storage, uint32_t len)
{
	script_list_delete(list);

    uint32_t *p = storage;
    uint32_t *p_head = p;

    char name[256] = { 0 };
    char text[1024] = { 0 };

    while(p - storage < len)
    {
    	p_head = p;

        uint32_t name_len = *p++;
        if(name_len > 256 || name_len == 0)
        {
        	lprintf("invalid name length, stop loading.\r\n");
        	break;
        }
        str_from_uint32_array(p, name, ((name_len - 1) >> 2) + 1);
        p += ((name_len - 1) >> 2) + 1;

        uint32_t text_len = *p++;
        if(text_len > 1024)
        {
        	lprintf("invalid text length, stop loading. \r\n");
        	break;
        }

        str_from_uint32_array(p, text, text_len);

        if(text_len == 0)
        {
        	text[0] = '\0';
        	p++;
        }
        else
        {
        	p += ((text_len - 1) >> 2) + 1;
        }

        uint32_t crc = crc32(0x93, p_head, p - p_head);
        /* read checksum */
        uint32_t checksum = *p++;

        if(crc != checksum)
        {
        	lprintf("cannot load script: checksum error\r\n");
        }
        else
        {
        	script_add(list, name, text);
        	lprintf("loaded script: %s\r\n", name);
        }
    }

    return list;
}

script_t *script_find(scripts_list_t *list, char *name, script_t **prev_script)
{
    script_t *current = list->script;
    script_t *prev = NULL;

    while(current)
    {
        if(strcmp(current->name, name) == 0)
        {
        	if(prev_script)
        	{
        		*prev_script = prev;
        	}

            return current;
        }

        prev = current;

        current = current->next;
    }

    return NULL;
}

script_t *script_append(script_t *script, char *line)
{
    char *reallocated = script_realloc(script->text, strlen(line) + script->text_len + 1);

    if(!reallocated)
    {
        return NULL;
    }
    else
    {
        script->text = reallocated;
        script->text_len += strlen(line);
        strcat(script->text, line);

        return script;
    }
}

script_t *script_rename(script_t *script, char *name)
{
	script_free(script->name);
    script->name_len = (uint32_t)strlen(name);

    script->name = script_malloc(sizeof(char) * (script->name_len + 1));
    if(!script->name)
    {
        return NULL;
    }

    strcpy(script->name, name);

    return script;
}

script_t *script_clear(script_t *script)
{
    /* release memory */
	script_free(script->text);

    /* set length to zero */
    script->text_len = 0;

    /* allocate one space for null char */
    script->text = script_malloc(sizeof(char));

    if(!script->text)
    {
        return NULL;
    }

    /* write null char */
    script->text[0] = '\0';

    return script;
}

void script_list_delete(scripts_list_t *list)
{
	script_t *current = script_list.script;

	while(current)
	{
		script_t *next = current->next;

		script_free(current->name);
		script_free(current->text);
		script_free(current);

		current = next;
	}

	list->script = NULL;
}

/*script_t *script_read_lines(script_t *script, script_line_func_t func)
{
	char *ptr = script->text;
	char line[1024] = { 0 };
	uint32_t count = 0;

	while(*ptr != '\0')
	{
		line[count++] = *ptr;

		if(*ptr == '\r')
		{
			line[count] = '\0';
			count = 0;
			func(line);
		}

		ptr++;
	}

	return script;
}*/
