/*
 * Copyright 2014, 2015 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the author nor the names of its contributors may be
 *    used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 *  This is a simple implementation of circular buffer for any data type.
 */

#ifndef __CIRCULAR_BUFFER_H__
#define __CIRCULAR_BUFFER_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

typedef uint32_t cb_size_t;

/**
 *  Declare a specific type of circular buffer type.
 *
 *  @param __type The data type
 *  @param __name Type name
 */
#define CB_DECLARE( __type)                                                 \
struct                                                                      \
{                                                                           \
    __type   *buffer;                                                       \
    size_t    dataSize;                                                     \
    cb_size_t bufferLength, writePosition, readPosition;                    \
    bool      filled;                                                       \
}

/**
 *  Initialize a circular buffer
 *
 *  @param __buffer The pointer to the buffer
 *  @param __type   Type name of data
 *  @param __length Length of the buffer
 */
#define CB_INIT(__buffer, __type, __length, __malloc)                       \
do {                                                                        \
    (__buffer)->writePosition = 0;                                          \
    (__buffer)->readPosition = 0;                                           \
    (__buffer)->filled = false;                                             \
    (__buffer)->bufferLength = __length;                                    \
    (__buffer)->dataSize = sizeof(__type);                                  \
    (__buffer)->buffer =                                                    \
        __malloc(sizeof(__type) * (__buffer)->bufferLength);                \
} while(0)

/**
 *  Append new data to the circular buffer
 *
 *  @param __buffer The pointer to the buffer
 *  @param __data   Data
 */
#define CB_APPEND(__buffer, __data)                                         \
do {                                                                        \
    if((__buffer)->writePosition == (__buffer)->bufferLength)               \
    {                                                                       \
        (__buffer)->writePosition = 0;                                      \
        (__buffer)->filled = true;                                          \
    }                                                                       \
    (__buffer)->buffer[(__buffer)->writePosition++] = __data;               \
} while(0)

/**
 *  Fill a given sequence in linear address space with
 *  the data in the circular buffer.
 *
 *  @param __buffer   The pointer to the circular buffer
 *  @param __sequence The pointer to the linear address space sequence
 */
#define CB_FILL_SEQUENCE(__buffer, __sequence, __memcpy)                    \
do {                                                                        \
    if(!(__buffer)->filled)                                                 \
        __memcpy((__sequence),                                              \
                 (__buffer)->buffer,                                        \
                 (__buffer)->writePosition * (__buffer)->dataSize);         \
    else                                                                    \
    {                                                                       \
        __memcpy((__sequence),                                              \
                 (__buffer)->buffer + (__buffer)->writePosition,            \
                 ((__buffer)->bufferLength - (__buffer)->writePosition)     \
                 * (__buffer)->dataSize);                                   \
                                                                            \
        __memcpy((__sequence) + (__buffer)->bufferLength                    \
                 - (__buffer)->writePosition,                               \
                 (__buffer)->buffer,                                        \
                 (__buffer)->writePosition * (__buffer)->dataSize);         \
    }                                                                       \
} while(0)

/**
 *  Get the valid length of the circular buffer
 *
 *  @param __buffer The pointer to the circular buffer
 */
#define CB_VALID_LENGTH(__buffer)   \
    ((__buffer)->filled ?           \
     (__buffer)->bufferLength :     \
     (__buffer)->writePosition)

/**
 *  Release the memory of the circular buffer
 *
 *  @param __buffer The pointer to the buffer
 */
#define CB_RELEASE(__buffer, __free_func) __free_func((__buffer)->buffer)

#define LOG_LENGTH 512

int log_init(void);
int log_append(char *str);
int print_log(void);

static int lprintf(char *fmt, ...)
{
    int i, count;
    char log_buf[LOG_LENGTH];

    va_list args;
    va_start(args, fmt);

    count = vsnprintf(log_buf, LOG_LENGTH, fmt, args);
    va_end(args);

    log_append(log_buf);

    return count;
}

#endif /* __CIRCULAR_BUFFER_H__ */
