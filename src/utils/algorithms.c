/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "../hw/sharc_hw.h"
#include "../motor_control.h"

void svpwm(const uint32_t pwm_period,
           const float alpha, const float beta,
           float *Taon, float *Tbon, float *Tcon)
{
    uint8_t  a, b, c, sector;
    float    temp_a, temp_b, temp_c, t1, t2;

#define SQRT3       1.7320508075688772f
#define SQRT3_HALF  0.8660254037844386f

    const int32_t pwm_half = pwm_period >> 1;

    const float sqrt3_alpha     = alpha * SQRT3;
    const float alpha_1p5       = alpha * 1.5f;
    const float sqrt3_half_beta = beta  * SQRT3_HALF;

    /* determine sector number */
    a = beta > 0.0f ? 1 : 0;
    b = ((-beta + sqrt3_alpha) / 2.0f) > 0.0f ? 1 : 0;
    c = ((-beta - sqrt3_alpha) / 2.0f) > 0.0f ? 1 : 0;

    /* inverse clarke transform */
#define CALCULATE_X ((( SQRT3 * beta)                / 3.0f) * pwm_period)
#define CALCULATE_Y ((( alpha_1p5 + sqrt3_half_beta) / 3.0f) * pwm_period)
#define CALCULATE_Z (((-alpha_1p5 + sqrt3_half_beta) / 3.0f) * pwm_period)

    /* calculate sector number */
    sector = (c << 2) + (b << 1) + a;
    switch(sector)
    {
        case 1: t1 =  CALCULATE_Z; t2 =  CALCULATE_Y; break;
        case 2: t1 =  CALCULATE_Y; t2 = -CALCULATE_X; break;
        case 3: t1 = -CALCULATE_Z; t2 =  CALCULATE_X; break;
        case 4: t1 = -CALCULATE_X; t2 =  CALCULATE_Z; break;
        case 5: t1 =  CALCULATE_X; t2 = -CALCULATE_Y; break;
        case 6: t1 = -CALCULATE_Y; t2 = -CALCULATE_Z; break;

        /* should never get here  */
        default: t1 = 0.0f; t2 = 0.0f;
    }

#undef CALCULATE_X
#undef CALCULATE_Y
#undef CALCULATE_Z

    /* calculate null vector duty-time */
    temp_a = ((pwm_period) - t1 - t2) / 2.0f;
    temp_b = temp_a + t1;
    temp_c = temp_b + t2;

    /* calculate PWM duty-time according to sector number */
    switch(sector)
    {
        case 1: *Taon = temp_b; *Tbon = temp_a; *Tcon = temp_c; break;
        case 2: *Taon = temp_a; *Tbon = temp_c; *Tcon = temp_b; break;
        case 3: *Taon = temp_a; *Tbon = temp_b; *Tcon = temp_c; break;
        case 4: *Taon = temp_c; *Tbon = temp_b; *Tcon = temp_a; break;
        case 5: *Taon = temp_c; *Tbon = temp_a; *Tcon = temp_b; break;
        case 6: *Taon = temp_b; *Tbon = temp_c; *Tcon = temp_a; break;

        /* should never get here */
        default: *Taon = 0.0f; *Tbon = 0.0f; *Tcon = 0.0f;
    }

    /* linear transform */
    temp_a = (*Taon - pwm_half) * SQRT3;
    temp_b = (*Tbon - pwm_half) * SQRT3;
    temp_c = (*Tcon - pwm_half) * SQRT3;

    *Taon = temp_a + pwm_half;
    *Tbon = temp_b + pwm_half;
    *Tcon = temp_c + pwm_half;

#undef SQRT3
}

float angle_normalize_deg(float angle)
{
    while(angle > 360.0) angle -= 360.0;
    while(angle < 0.0  ) angle += 360.0;

    return angle;
}

fir_filter_t *fir_filter_init(fir_filter_t *fir, float *coeffs, int len)
{
    fir->coeffs = malloc(sizeof(float) * len);
    fir->data = malloc(sizeof(float) * len);
    fir->len = len;

    for(int i = 0; i < len; i++)
    {
        fir->data[i] = 0.0f;
        fir->coeffs[i] = coeffs[i];
    }

    return fir;
}

float fir_update(fir_filter_t *fir, float data)
{
    float response = 0.0;

    for(int i = 0; i < fir->len - 1; i++)
    {
        fir->data[i] = fir->data[i + 1];
    }

    fir->data[fir->len - 1] = data;

    for(int i = 0; i < fir->len; i++)
    {
        response += fir->data[i] * fir->coeffs[i];
    }

    return response;
}

float iir_filter_update(iir_filter_t *filter, float x)
{
    float y =
    filter->a0 * x +
    filter->a1 * filter->x_1 +
    filter->a2 * filter->x_2 +
    filter->b1 * filter->y_1 +
    filter->b2 * filter->y_2;

    filter->x_2 = filter->x_1;
    filter->x_1 = x;
    filter->y_2 = filter->y_1;
    filter->y_1 = y;

    return y;
}

pid_controller_t *pid_controller_update_settings(pid_controller_t *pid, pid_settings_t *settings)
{
	pid->p = settings->p;
	pid->i = settings->i;
	pid->d = settings->d;

	return pid;
}

pid_settings_t *pid_controller_extract_settings(pid_controller_t *pid, pid_settings_t *settings)
{
	settings->p = pid->p;
	settings->i = pid->i;
	settings->d = pid->d;

	return settings;
}

pid_controller_t *pid_controller_init(pid_controller_t *pid, float p, float i, float d)
{
    pid->integral = 0.0f;

    pid->p = p;
    pid->i = i;
    pid->d = d;
    pid->error_prev = 0.0f;
    pid->p_prime = 0.0f;
    pid->i_prime = 0.0f;

    return pid;
}

pid_controller_t *pid_controller_reset(pid_controller_t *pid)
{
    pid->integral = 0.0f;
    pid->error_prev = 0.0f;
    pid->p_prime = 0.0f;
    pid->i_prime = 0.0f;

    return pid;
}

float pi_update(pid_controller_t *pid,
		float error,
        float dt, float min, float max,
        float int_min, float int_max,
        float feed_forward)
{
    pid->integral += error * dt;


    /* calculate P term using the given P value and its compensation term */
    float p = pid->p + pid->p_prime;
    p = p < 0.0 ? 0.0 : p;

    float i = pid->i + pid->i_prime;
    i = i < 0.0 ? 0.0 : i;

    float p_out = error *p;

#if 0
#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
    /* integrator dynamic clamping */
    float int_min_new = MIN(int_min - p_out, 0.0f);
    float int_max_new = MAX(int_max - p_out, 0.0f);
    pid->integral = pid->integral > int_max_new ? int_max_new : pid->integral;
    pid->integral = pid->integral < int_min_new ? int_min_new : pid->integral;
#else
    pid->integral = pid->integral > int_max ? int_max : pid->integral;
    pid->integral = pid->integral < int_min ? int_min : pid->integral;
#endif

    float result = p_out + pid->integral * i + feed_forward;

    result = result > max ? max : result;
    result = result < min ? min : result;

    return result;
}

float pid_update(pid_controller_t *pid,
		float error,
		float diff,
        float dt, float min, float max,
        float int_min, float int_max,
        float feed_forward)
{
    /* first calculate PI update value */
	float pi_value = pi_update(
            pid, error, dt,
            min, max,
            int_min, int_max,
            feed_forward - pid->d * diff
    );

	/* then clamp output */
	if(pi_value > max) pi_value = max;
	if(pi_value < min) pi_value = min;

    return pi_value;
}

float pid_pdff_update(pid_controller_t *pdff, float sp, float pv,
		float dt, float min, float max, float int_min, float int_max, float feed_forward)
{
	float error = sp - pv;
	pdff->integral += error * dt;
	pdff->integral = pdff->integral > int_max ? int_max : pdff->integral;
	pdff->integral = pdff->integral < int_min ? int_min : pdff->integral;

	float P = pdff->p * pv;

	float result = pdff->integral * pdff->i - P + feed_forward;

    result = result > max ? max : result;
    result = result < min ? min : result;

    return result;
}
//

/**
 *  find the fuzzy logic level from a given fuzzy logic rule using given
 *  error and change of error
 *
 *  @param e                error
 *  @param ec               change of error
 *  @param fuzzy_logic_rule fuzzy logic rule table
 *
 *  @return fuzzy logic level
 */
int fuzzy_logic_find(int e, int ec, int *fuzzy_logic_rule)
{
    /* make sure logic table is within boundary */
    e =  e < NB ? NB : e;
    e =  e > PB ? PB : e;

    ec = ec < NB ? NB : ec;
    ec = ec > PB ? PB : ec;

    /* shift e and ec by 3 due to negative quantity in logic rules */
    return (int)fuzzy_logic_rule[(((int)e) + 3) * 7 + (((int)ec) + 3)];
}

/**
 *  calculate the membership of the quantity x from the given boundary
 *
 *  @param nb negative big boundary
 *  @param pb positive big boundary
 *  @param x  quantity x
 *
 *  @return fuzzy logic membership
 */
int fuzzy_logic_membership(float nb, float pb, float x)
{
    int level = (((int)(((x - nb) / (pb - nb)) * 7)) - 3);

    level =  level < NB ? NB : level;
    level =  level > PB ? PB : level;

    return level;
}

/**
 *  defuzzify a given fuzzy level into quantity
 *
 *  @param nb    negative big
 *  @param pb    positive big
 *  @param level fuzzy logic level
 *
 *  @return defuzzified quantity
 */
float fuzzy_logic_defuzzify(float nb, float pb, int level)
{
    return (3.0 * nb - level * nb + 3.0 * pb + level * pb) * 0.166666666666667;
}

#define ABS(x) ((x) > 0 ? (x) : -(x))


ab_observer_t *ab_observer_init(ab_observer_t *observer, float dt, float a, float b)
{
    observer->dt = dt;
    observer->a = a;
    observer->b = b;
    observer->xk_1 = 0.0f;
    observer->vk_1 = 0.0f;
    observer->output_x_dot = 0.0f;
    observer->xm = observer->xm1 = 0;

    return observer;
}

ab_observer_t *ab_observer_update(ab_observer_t *observer, float xm)
{
    float xk = observer->xk_1 + (observer->vk_1 * observer->dt);
    float vk = observer->vk_1;

    float rk = xm - xk;

    xk += observer->a * rk;
    vk += (observer->b * rk) / observer->dt;

    observer->xk_1 = xk;
    observer->vk_1 = vk;

    return observer;
}

float ab_observer_differential_update(ab_observer_t *observer, int64_t xm)
{
    /* save measurement */
    observer->xm = xm;

    /* find xm dot */
    float diff = observer->xm - observer->xm1;

    /* observer xm dot dot */
    ab_observer_update(observer, diff);

    /* integral xm dot dot -> xm dot */
    observer->output_x_dot += observer->vk_1;

    observer->xm1 = observer->xm;

    return observer->output_x_dot;
}

differentiator_int64_t *differentiator_int64_init(differentiator_int64_t *d)
{
    for(int i = 0; i < 5; i++)
    {
        d->z[i] = 0;
    }

    return d;
}

differentiator_float_t *differentiator_float_init(differentiator_float_t *d)
{
    for(int i = 0; i < 5; i++)
    {
        d->z[i] = 0.0;
    }

    return d;
}

float differential_int64(differentiator_int64_t *diff, int64_t x)
{
    int64_t min = diff->z[0];

    for(int i = 0; i < 4; i++)
    {
        diff->z[i] = diff->z[i + 1];
        if(diff->z[i] < min) min = diff->z[i];
    }

    min = x < min ? x : min;
    diff->z[4] = x;

    const float a = diff->z[0] - min;
    const float b = diff->z[1] - min;
    const float c = diff->z[3] - min;
    const float d = diff->z[4] - min;

    return (1 / 12.0) * (-d + 8.0 * c - 8.0 * b + a);
}

float differential_float(differentiator_float_t *diff, float x)
{
    for(int i = 0; i < 4; i++)
    {
        diff->z[i] = diff->z[i + 1];
    }

    diff->z[4] = x;

    return (1 / 12.0) * (-diff->z[4] + 8.0 * diff->z[3] - 8.0 * diff->z[1] + diff->z[0]);
}

char get_hex(int num)
{
    static char a[] = "0123456789abcdef";
    return a[num];
}

void print_variable_record(float *buffer, int len)
{
	uint32_t bytes[4];

	for(int i = 0; i < len; i++)
	{
		uint32_t word = *((uint8_t *)(buffer) + i);

		bytes[0] = (word & 0x000000ff);
		bytes[1] = (word & 0x0000ff00) >> 8;
		bytes[2] = (word & 0x00ff0000) >> 16;
		bytes[3] = (word & 0xff000000) >> 24;

		for(int j = 3; j >= 0; j--)
		{
			uart_char_send(get_hex((bytes[j] >> 4) & 0xf));
			uart_char_send(get_hex(bytes[j] & 0xf));
		}
	}
}

iir_filter_t *iir_filter_reset(iir_filter_t *filter)
{
	filter->x_1 = 0.0f;
	filter->x_2 = 0.0f;
	filter->y_1 = 0.0f;
	filter->y_2 = 0.0f;

	return filter;
}

iir_filter_t *iir_filter_init(iir_filter_t *filter, float a0, float a1, float a2, float b1, float b2)
{
	filter->a0 = a0;
	filter->a1 = a1;
	filter->a2 = a2;
	filter->b1 = b1;
	filter->b2 = b2;

	return filter;
}

#define M_PI 3.1415926

void calculate_iir_coeff(float bw, float cf,
                         float *a0, float *a1, float *a2,
                         float *b1, float *b2)
{
    const float r = 1.0f - 3.0f * bw;
    const float k = (1.0f - 2.0f * r * cos(2.0f * M_PI * cf) + pow(r, 2.0f)) /
    (2.0f - 2.0f * cos(2.0f * M_PI * cf));

    *a0 = k;
    *a1 = -2.0f * k * cos(2.0f * M_PI * cf);
    *a2 = k;
    *b1 = 2.0f * r * cos(2.0f * M_PI * cf);
    *b2 = -pow(r, 2.0f);
}

// -------------

iir_filter_nth_t *iir_filter_nth_init(iir_filter_nth_t *filter, float *a, float *b, int order)
{
    filter->order = order;

    filter->a = malloc(sizeof(float) * (order + 1));
    filter->b = malloc(sizeof(float) * (order + 1));
    filter->z = malloc(sizeof(float) * order);

    memcpy(filter->a, a, sizeof(float) * (order + 1));
    memcpy(filter->b, b, sizeof(float) * (order + 1));
    memset(filter->z, 0, sizeof(float) * order);

    return filter;
}

float iir_filter_nth_update(iir_filter_nth_t *filter, float x)
{
    for(int k = filter->order; k > 0; k--)
    {
        filter->z[k] = filter->z[k - 1];
    }
    filter->z[0] = x;

    for(int k = 1; k <= filter->order; k++)
    {
        filter->z[0] -= filter->a[k] * filter->z[k];
    }

    float y = 0;
    for(int k = 0; k <= filter->order; k++)
    {
        y += filter->b[k] * filter->z[k];
    }

    return y;
}

average_sampler_t *average_sampler_init(average_sampler_t *sampler, int len)
{
    sampler->x_sum = 0.0f;
    sampler->current_sample = 0;
    sampler->sample_count = len;

    return sampler;
}

float average_sampler_update(average_sampler_t *sampler, float x, int *flag)
{
	sampler->current_sample++;
    sampler->x_sum += x;

    if(sampler->current_sample >= sampler->sample_count)
    {
    	sampler->x = sampler->x_sum / sampler->sample_count;
    	sampler->x_sum = 0;
    	sampler->current_sample = 0;

    	if(flag)
    	{
    		*flag = 1;
    	}
    }

    return sampler->x;
}

/* ------ */
median_filter_t *median_filter_init(median_filter_t *filter, int len)
{
    /* filter length shall be an even number */
    if(len % 2 != 1)
    {
        return NULL;
    }

    filter->len = len;
    filter->buf_samples = malloc(sizeof(int64_t) * len);
    filter->samples = malloc(sizeof(int64_t) * len);

    /* memory allocation failed */
    if(!filter->samples || !filter->buf_samples)
    {
        return NULL;
    }

    /* clear sample buffer */
    for(int i = 0; i < len; i++)
    {
        filter->samples[i] = 0;
        filter->buf_samples[i] = 0;
    }

    return filter;
}

static inline
void __int64_swap(int64_t *array, int a, int b)
{
    int64_t temp = array[a];
    array[a] = array[b];
    array[b] = temp;
}

static inline
int __med_filter_partition(int64_t *array, int left, int right)
{
    if (array == NULL)
    {
        return -1;
    }

    int pos = right;
    right--;

    while (left <= right)
    {
        while (left < pos && array[left] <= array[pos])
        {
            left++;
        }

        while (right >= 0 && array[right] > array[pos])
        {
            right--;
        }

        if (left >= right)
        {
            break;
        }

        __int64_swap(array, left, right);
    }

    __int64_swap(array, left, pos);

    return left;
}

static inline
int64_t __med_filter_find_med(int64_t *array, int size)
{
    if (array == NULL || size <= 0)
        return -1;

    int left = 0;
    int right = size - 1;
    int midPos = right / 2;
    int index = -1;

    while (index != midPos)
    {
        index = __med_filter_partition(array, left, right);

        if (index < midPos)
        {
            left = index + 1;
        }
        else if (index > midPos)
        {
            right = index - 1;
        }
        else
        {
            break;
        }
    }

    return array[index];
}

int64_t median_filter_update(median_filter_t *filter, int64_t sample)
{
    /* update samples */
    for(int i = 0; i < filter->len - 1; i++)
    {
        filter->samples[i] = filter->samples[i + 1];
    }

    filter->samples[filter->len - 1] = sample;

    for(int i = 0; i < filter->len; i++)
    {
        filter->buf_samples[i] = filter->samples[i];
    }

    return __med_filter_find_med(filter->buf_samples, filter->len);
}

linear_interpolator *linear_init(linear_interpolator *lin,
                                 float x1, float y1, float x2, float y2)
{
    /* copy properties */
    lin->x1 = x1;
    lin->x2 = x2;
    lin->y1 = y1;
    lin->y2 = y2;

    if(isnan(x1) || isnan(x2) || isnan(y1) || isnan(y2) ||
    		(fabsf(x2 - x1) < 0.001))
    {
    	lin->m = 0.0f;
    	lin->b = 0.0f;
    }
    else
    {
        /* calculate slope and offset */
        lin->m = (y2 - y1) / (x2 - x1);
        lin->b = y1 - x1 * lin->m;
    }

    return lin;
}

