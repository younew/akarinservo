#ifndef MOTION_PLANNER_H_
#define MOTION_PLANNER_H_

#include <stddef.h>
#include <stdint.h>

typedef struct
{
    float x, y;
} vector2f_t;

typedef struct
{
    float v2;
    float dt1, dt2, dt3;

    float current_pos, target_pos;

    int tick;
    int total_tick;
    float dt;

    float vmax;
    float a_acc, a_dec;

    float pos_output;

    float x1, x2;
    float cx1, cx2;

    float normalized_time;

    float dir;

} motion_controller_t;


motion_controller_t *motion_control_tick(motion_controller_t *ctrl);

motion_controller_t *motion_prepare(
    motion_controller_t *c,
    float current_pos, float target_pos, float acc_peak, float vel_peak,
    float dt);

#endif /* MOTION_PLANNER_H_ */
