#include "motion_planner.h"
#include <math.h>
#include <stdio.h>

#define FLOAT_IS_ZERO(x) (fabsf(x) < 0.0001f)

#define M_PI        3.14159265358979323846264338327950288
#define M_2PI       6.283185307179586
#define M_HALF_PI   1.570796326794897

float p2(float t, float x0, float v0, float a)
{
    return x0 + v0 * t + 0.5f * a * t * t;
}

motion_controller_t *motion_prepare(
    motion_controller_t *c,
    float current_pos, float target_pos, float acc_peak, float vel_peak,
    float dt)
{
    c->current_pos = current_pos;
    c->target_pos = target_pos;
    c->tick = 0;

    if(target_pos < current_pos)
    {
        float tmp = current_pos;
        current_pos = target_pos;
        target_pos = tmp;
        c->dir = -1;
    }
    else
    {
        c->dir = 1;
    }

    float v = vel_peak;
    float v0 = 0.0f;
    float a_acc = acc_peak;
    float a_dec = acc_peak;
    float x0 = 0;
    float x_goal = target_pos - current_pos;

    c->dt1 = (v - v0) / a_acc;
    float dx1 = p2(c->dt1, 0, v0, a_acc);
    c->dt3 = v / a_dec;
    float dx3 = p2(c->dt3, 0, v, -a_dec);
    c->dt2 = (x_goal - (x0 + dx1 + dx3)) / v;

    float t3 = c->dt1 + c->dt2 + c->dt3;

    if(c->dt2 < 0)
    {
        v = sqrtf(acc_peak * (x_goal - x0) + 0.5 * v0 * v0);
        t3 = (v - v0) / a_acc + v / a_dec;
        c->dt1 = t3 / 2.0f;
        c->dt2 = 0;
    }

    c->v2 = a_acc * c->dt1;
    c->total_tick = t3 / dt;
    c->dt = dt;
    c->vmax = v;

    c->a_acc = a_acc;
    c->a_dec = a_dec;

    c->cx1 = p2(c->dt1, 0, 0, c->a_acc);
    c->cx2 = p2(c->dt2, c->cx1, c->vmax, 0);

    return c;
}

motion_controller_t *motion_control_tick(motion_controller_t *c)
{
    c->tick++;

    float t = c->tick * c->dt;
    float pos_out;

    if(t <= c->dt1 + c->dt)
    {
        c->x1 = p2(t, 0, 0, c->a_acc);
        c->x2 = c->x1;

        pos_out = c->x1;
    }
    else if(t < (c->dt1 + c->dt2))
    {
        c->x2 = p2(t - c->dt1, c->cx1, c->vmax, 0);

        pos_out = c->x2;
    }
    else
    {
        pos_out = p2(t - (c->dt1 + c->dt2), c->cx2, c->v2, -c->a_dec);
    }

    c->pos_output = c->current_pos + c->dir * pos_out;

    if(c->tick >= c->total_tick)
    {
        return NULL;
    }
    else
    {
    	c->normalized_time = c->pos_output / (c->target_pos - c->current_pos);
        return c;
    }
}
