/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef KINEMATICS_DELTA_KINEMATICS_H_
#define KINEMATICS_DELTA_KINEMATICS_H_

#include "../kalman/kalman_filter.h"

#define AXIS_A	'a'
#define AXIS_B	'b'
#define AXIS_C	'c'

#define DELTA_FLT_EPS 0.001

extern float _delta_bottom_z, _delta_top_z;
extern float _delta_radius;

extern INSVector3f delta_current_position;
extern INSVector3f delta_prev_pos_cmd;

typedef struct
{
    INSVector3f pts[4];
} bezier3d_3rd_interpolator_t;

typedef struct
{
	float x, y, z;
} delta_kinematics_test_t;

static inline float delta_bottom_z(void)
{
	return _delta_bottom_z;
}

static inline float delta_top_z(void)
{
	return _delta_top_z;
}

static inline float delta_radius(void)
{
	return _delta_radius;
}

static inline int bound(INSVector3f pt)
{
	if(pt.x > 220 || pt.x < -220 || pt.y > 220 || pt.y < -220 || pt.z > -100 || pt.z < -250)
	{
		return 1;
	}

	return 0;
}

INSVector3f bezier3d_3rd(bezier3d_3rd_interpolator_t *b3d, float t);

int delta_fwd_kinematics(float theta1, float theta2, float theta3, float *x0, float *y0, float *z0);

int delta_inverse_kinematics(float x0, float y0, float z0, float *theta1, float *theta2, float *theta3);

void delta_calculate_bounds(float s);

int delta_kinematics_selftest(void);

INSVector3f interpolate_3d_linear(INSVector3f start, INSVector3f end, float t);

#endif /* KINEMATICS_DELTA_KINEMATICS_H_ */
