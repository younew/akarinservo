/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdarg.h>

#include "kalman/kalman_filter.h"
#include "hw/encoder.h"

#include "command/command.h"

#include "db/kvmap.h"

#include "utils/log.h"
#include "script.h"
#include "kinematics/delta_kinematics.h"

extern int motion_type;
extern int motion_pause_length;
extern int motion_pause_count;

int seconds = 0;

static int initialize_kvmap_ctrl(void)
{
    controller_kvmap = kvmap_create(17, ctrl_fram_write_word, ctrl_fram_read_word);

#define CTRL_VAR_LINK(major, minor, ptr, len) \
        kvmap_link_variable(controller_kvmap, major, minor, ptr, len);

    /** group 90: controller settings */
    /* parameter 90:1, local address */
    CTRL_VAR_LINK(90, 1, &mc_session.controller_configs.local_address, 1);
    /* parameter 90:2, controller name */
    CTRL_VAR_LINK(90, 2, &mc_session.controller_configs.name, CTRL_NAME_LEN);

    /** group 91: control instant settings */
    CTRL_VAR_LINK(91, 2, &mc_session.controller_configs.out_of_control_thres, 1);
    CTRL_VAR_LINK(91, 3, &mc_session.controller_configs.pulse_input_mode, 1);
    CTRL_VAR_LINK(91, 4, &mc_session.controller_configs.pulse_step_amount, 1);
    CTRL_VAR_LINK(91, 5, &mc_session.controller_configs.dio_config.dir, 1);
    CTRL_VAR_LINK(91, 6, &mc_session.controller_configs.dio_config.usage, 1);

    /** group 10: current sensor settings */
    /* parameter 10:1, 10:2, current sensor bias */
    CTRL_VAR_LINK(10, 1, &mc_session.controller_configs.hall_a_bias, 1);
    CTRL_VAR_LINK(10, 2, &mc_session.controller_configs.hall_b_bias, 1);
    /* parameter 10:3, 10:4, current sensor coefficients */
    CTRL_VAR_LINK(10, 3, &mc_session.controller_configs.hall_a_coeff, 1);
    CTRL_VAR_LINK(10, 4, &mc_session.controller_configs.hall_b_coeff, 1);

    /** group 100: scripts */
    CTRL_VAR_LINK(100, 1, scripts_storage, SCRIPT_STORAGE_LEN);

    return 0;
}

static int initialize_kvmap_encoder(void)
{
    encoder_kvmap = kvmap_create(17, encoder_fram_write_word, encoder_fram_read_word);

#define ENCODER_VAR_LINK(major, minor, ptr, len) \
        kvmap_link_variable(encoder_kvmap, major, minor, ptr, len);

    /** group 100: motor & encoder settings */
    ENCODER_VAR_LINK(100, 1, &mc_session.encoder_configs.poles, 1);
    ENCODER_VAR_LINK(100, 2, &mc_session.encoder_configs.encoder_offset, 1);
    ENCODER_VAR_LINK(100, 3, &mc_session.encoder_configs.rated_rpm, 1);
    ENCODER_VAR_LINK(100, 4, &mc_session.encoder_configs.torque_coeff, 1);
    ENCODER_VAR_LINK(100, 5, &mc_session.encoder_configs.motor_current_protect_limit, 1);

    ENCODER_VAR_LINK(100, 20, &mc_session.encoder_configs.defulat_acc, 1);
    ENCODER_VAR_LINK(100, 21, &mc_session.encoder_configs.max_acc, 1);

    /** group 101: position controller settings */
    ENCODER_VAR_LINK(101, 1, &mc_session.position_controller.p, 1);
    ENCODER_VAR_LINK(101, 2, &mc_session.position_controller.i, 1);
    ENCODER_VAR_LINK(101, 3, &mc_session.position_controller.d, 1);

    /** group 102: velocity controller settings */
    ENCODER_VAR_LINK(102, 1, &mc_session.velocity_controller.p, 1);
    ENCODER_VAR_LINK(102, 2, &mc_session.velocity_controller.i, 1);
    ENCODER_VAR_LINK(102, 3, &mc_session.velocity_controller.d, 1);
    ENCODER_VAR_LINK(102, 4, &mc_session.encoder_configs.vel_feedforward, 1);
    ENCODER_VAR_LINK(102, 5, &mc_session.encoder_configs.rotor_vel_limit, 1);

    /** group 103: torque controller settings */
    ENCODER_VAR_LINK(103, 1, &mc_session.torque_controller.p, 1);
    ENCODER_VAR_LINK(103, 2, &mc_session.torque_controller.i, 1);
    ENCODER_VAR_LINK(103, 3, &mc_session.torque_controller.d, 1);
    ENCODER_VAR_LINK(103, 4, &mc_session.encoder_configs.torque_feedforward, 1);
    ENCODER_VAR_LINK(103, 5, &mc_session.encoder_configs.torque_limit_ccw, 1);
    ENCODER_VAR_LINK(103, 6, &mc_session.encoder_configs.torque_limit_cw, 1);

    /** group 104: magnetic flux controller settings */
    ENCODER_VAR_LINK(104, 1, &mc_session.mag_flux_controller.p, 1);
    ENCODER_VAR_LINK(104, 2, &mc_session.mag_flux_controller.i, 1);
    ENCODER_VAR_LINK(104, 3, &mc_session.mag_flux_controller.d, 1);

    /** group 105: load inertia estimator settings */
    ENCODER_VAR_LINK(105, 1, &mc_session.encoder_configs.inertia_rotor_calibrated, 1);
    ENCODER_VAR_LINK(105, 2, &mc_session.encoder_configs.inertia_rotor_cal_torque, 1);
    ENCODER_VAR_LINK(105, 3, &mc_session.encoder_configs.inertia_rotor_cal_duration, 1);

    /** group 106: cogging torque compensation */
    ENCODER_VAR_LINK(106, 1, &mc_session.encoder_configs.cogging_torque, 512);

    /** group 107: reference config settings */
    ENCODER_VAR_LINK(107, 1, &mc_session.ref_config.ref_unit, 1);
    ENCODER_VAR_LINK(107, 2, &mc_session.ref_config.elec_gear_n, 1);
    ENCODER_VAR_LINK(107, 3, &mc_session.ref_config.elec_gear_m, 1);
    ENCODER_VAR_LINK(107, 4, &mc_session.ref_config.enc_count_per_ref_unit, 1);

    /** group 110: friction compensation */
    ENCODER_VAR_LINK(110, 1, &mc_session.encoder_info.friction_comp_rpm_h, 1);
    ENCODER_VAR_LINK(110, 2, &mc_session.encoder_info.friction_comp_torque_h, 1);
    ENCODER_VAR_LINK(110, 3, &mc_session.encoder_info.friction_comp_rpm_l, 1);
    ENCODER_VAR_LINK(110, 4, &mc_session.encoder_info.friction_comp_torque_l, 1);

    /** group 110: zero offset */
    ENCODER_VAR_LINK(111, 1, &mc_session.x_offset, 2);
    ENCODER_VAR_LINK(111, 2, &mc_session.encoder_configs.direction, 1);

    return 0;
}

int initialize_kvmap(void)
{
    initialize_kvmap_ctrl();
    initialize_kvmap_encoder();

    kvmap_load_variables(controller_kvmap);
    kvmap_load_variables(encoder_kvmap);

    lprintf("controller name: %s\r\n", mc_session.controller_configs.name);

    return 0;
}

int initialize_variable_alias(void)
{
    const int group_ctrl        = group("ALL", "CTRL_FRAM",     NULL);
    const int group_encoder     = group("ALL", "ENC_FRAM",      NULL);
    const int group_performance = group("ALL", "PERFORMANCE",   NULL);
    const int group_sensor      = group("ALL", "SENSOR",        NULL);
    const int group_servo_var   = group("ALL", "SERVO_VAR",     NULL);
    const int group_scope       = group("ALL", "SCOPE",         NULL);
    const int group_motion      = group("ALL", "MOTION",        NULL);
    const int group_filter      = group("ALL", "FILTER",        NULL);
    const int group_vfd         = group("ALL", "VFD",           NULL);

    const int group_delta       = group("ALL", "DELTA",         NULL);

    alias_variable(&delta_current_position.x, "delta_x", FLOAT32, VAR_RO, group_delta);
    alias_variable(&delta_current_position.y, "delta_y", FLOAT32, VAR_RO, group_delta);
    alias_variable(&delta_current_position.z, "delta_z", FLOAT32, VAR_RO, group_delta);

    /*
     * friction compensation variables
     * --------------------------------
     * */
    alias_variable(&mc_session.encoder_info.friction_comp_rpm_h, "friction_comp_rpm_h", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_info.friction_comp_rpm_l, "friction_comp_rpm_l", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_info.friction_comp_torque_h, "friction_comp_torque_h", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_info.friction_comp_torque_l, "friction_comp_torque_l", FLOAT32, VAR_RW, group_encoder);

    /*
     * reference input settings
     * --------------------------------
     * */
    alias_variable(&mc_session.ref_config.ref_unit, "reference_unit", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.ref_config.elec_gear_n, "elec_gear_n", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.ref_config.elec_gear_m, "elec_gear_m", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.ref_config.enc_count_per_ref_unit, "enc_count_per_ref_unit", FLOAT32, VAR_RW, group_encoder);

    /*
     * session status related variables
     * --------------------------------
     * */
    alias_variable(&mc_session.state,   "state",    INT32, VAR_RW, group_performance);
    alias_variable(&mc_session.stop,    "stop",     INT32, VAR_RW, group_performance);
    alias_variable(&global_error,       "error",    INT32, VAR_RW, group_performance);

    alias_variable(&core_control_loop_enabled, "core_timer_enable", INT32, VAR_RW, group_performance);

    alias_variable(&echo_enable,  "echo",  INT32, VAR_RW, group_performance);
    alias_variable(&debug_enable, "debug", INT32, VAR_RW, group_performance);


    alias_variable(&mc_session.x_offset,   "x_offset",    ANGULAR_POS, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.direction, "direction", INT32, VAR_RW, group_encoder);


    /*
     * controller settings
     * -------------------
     * */
    alias_variable(&mc_session.controller_configs.out_of_control_thres, "out_of_control_thres", INT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.controller_configs.local_address, "local_address", INT32, VAR_RW, group_ctrl);

    /*
     * controller performance counters
     * -------------------------------
     * */
    alias_variable(&seconds, "seconds", INT32, VAR_RO, group_performance);
    alias_variable(&script_malloc_count, "script_malloc_count", INT32, VAR_RO, group_performance);

    alias_variable(&mc_session.performance.max, "cycle_max", INT32, VAR_RO, group_performance);
    alias_variable(&mc_session.performance.min, "cycle_min", INT32, VAR_RO, group_performance);
    alias_variable(&mc_session.performance.current, "cycle_current", INT32, VAR_RO, group_performance);

    alias_variable(&mc_session.performance.pos_loop_freq, "pos_loop_freq", INT32, VAR_RO, group_performance);
    alias_variable(&mc_session.performance.vel_loop_freq, "vel_loop_freq", INT32, VAR_RO, group_performance);

    alias_variable(&mc_session.encoder_misscode_count, "encoder_misscode_count", INT32, VAR_RO, group_performance);

    /*
     * controller digital I/O configs
     * ------------------------------
     * */
    alias_variable(&mc_session.controller_configs.dio_config.dir, "dio_dir", INT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.dio_state.dio_out, "dio_out", INT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.dio_state.dio_in, "dio_in", INT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.controller_configs.dio_config.usage, "dio_usage", INT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.dio_home_switch, "home_switch", INT32, VAR_RW, group_performance);

    /*
     * motor & encoder variables
     * -------------------------
     * */
    alias_variable(&mc_session.encoder_info.rev_pulse, "rev_pulse", INT32, VAR_RO, group_encoder);
    alias_variable(&mc_session.encoder_configs.poles, "poles", INT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.encoder_offset, "enc_off", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.rated_rpm, "rated_rpm", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.defulat_acc, "default_acc", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.max_acc, "max_acc", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.torque_coeff, "torque_coeff", FLOAT32, VAR_RW, group_encoder);

    /* measured electrical angle */
    alias_variable(&mc_session.electric_angle, "elec_angle", FLOAT32, VAR_RO, group_servo_var);

    /*
     * control loop related variables, set points and process values
     * -------------------------------------------------------------
     * */
    /* angular position set point */
    alias_variable(&mc_session.rot_pos_set,  "rot_pos_set",  ANGULAR_POS, VAR_RW, group_servo_var);
    /* angular position measured value */
    alias_variable(&mc_session.rot_pos_meas, "rot_pos_meas", ANGULAR_POS, VAR_RO, group_servo_var);
    /* measured output torque */
    alias_variable(&mc_session.torque_meas,  "torque_meas",  FLOAT32, VAR_RO, group_servo_var);
    /* torque set point(position servo mode) */
    alias_variable(&mc_session.torque_set,   "torque_set",   FLOAT32, VAR_RO, group_servo_var);
    /* magnetic flux command */
    alias_variable(&mc_session.flux_cmd,     "flux_cmd",     FLOAT32, VAR_RO, group_servo_var);
    /* measured magnetic flux value */
    alias_variable(&mc_session.flux_meas,    "flux",         FLOAT32, VAR_RO, group_servo_var);
    /* current command to SVPWM */
    alias_variable(&mc_session.current_cmd,  "current_cmd",  FLOAT32, VAR_RO, group_servo_var);
    /* measured angular velocity */
    alias_variable(&mc_session.rot_vel_meas, "rot_vel_meas", FLOAT32, VAR_RO, group_servo_var);
    /* angular velocity set point */
    alias_variable(&mc_session.rot_vel_set,  "rot_vel_set",  FLOAT32, VAR_RO, group_servo_var);
    /* rotary encoder raw reading */
    alias_variable(&mc_session.encoder_raw,  "encoder_raw",  INT32,   VAR_RO, group_sensor);

    /* torque set point(torque servo mode only) */
    alias_variable(&mc_session.free_run_torque, "free_q", FLOAT32, VAR_RW, group_servo_var);
    /* velocity set point(velocity servo mode only) */
    alias_variable(&mc_session.rotor_angular_vel_control, "rot_vel", FLOAT32, VAR_RW, group_servo_var);

    alias_variable(&mc_session.set_angle, "set_angle", FLOAT32, VAR_RW, group_servo_var);

    /*
     * control loop related variables: feed-forward, filters and limits
     * ----------------------------------------------------------------
     * */
    /* velocity feed-forward coefficient */
    alias_variable(&mc_session.encoder_configs.vel_feedforward, "vel_ff", FLOAT32, VAR_RW, group_encoder);
    /* torque feed-forward coefficient */
    alias_variable(&mc_session.encoder_configs.torque_feedforward, "torque_ff", FLOAT32, VAR_RW, group_encoder);

    /* velocity observer(from position command) */
    alias_variable(&mc_session.vel_ff_observer.a, "vel_ff_a", FLOAT32, VAR_RW, group_filter);
    alias_variable(&mc_session.vel_ff_observer.b, "vel_ff_b", FLOAT32, VAR_RW, group_filter);

    /* torque limit value */
    alias_variable(&mc_session.encoder_configs.torque_limit_ccw, "torque_limit_ccw", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.torque_limit_cw, "torque_limit_cw", FLOAT32, VAR_RW, group_encoder);
    /* velocity limit value */
    alias_variable(&mc_session.encoder_configs.rotor_vel_limit, "rot_vel_limit", FLOAT32, VAR_RW, group_encoder);

    /*
     * control loop related variables: performance
     * -------------------------------------------
     * */
    alias_variable(&mc_session.pos_error, "pos_error", FLOAT32, VAR_RO, group_servo_var);
    alias_variable(&mc_session.vel_error, "vel_error", FLOAT32, VAR_RO, group_servo_var);
    alias_variable(&mc_session.pos_derror, "pos_derror", FLOAT32, VAR_RO, group_servo_var);
    alias_variable(&mc_session.pos_set_dd, "pos_set_dd", FLOAT32, VAR_RW, group_servo_var);

    alias_variable(&mc_session.ang_vel_display, "ang_vel_display", FLOAT32, VAR_RO, group_sensor);
    alias_variable(&mc_session.torque_display, "torque_display", FLOAT32, VAR_RO, group_sensor);

    alias_variable(&mc_session.current_meas_filtered, "motor_current_meas", FLOAT32, VAR_RO, group_sensor);
    alias_variable(&mc_session.encoder_configs.motor_current_protect_limit, "motor_current_protect_limit", FLOAT32, VAR_RW, group_encoder);

    /*
     * current sensors
     * ---------------
     * */
    /* current sensor calibrations */
    alias_variable(&mc_session.controller_configs.hall_a_bias, "hall.a.bias", INT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.controller_configs.hall_b_bias, "hall.b.bias", INT32, VAR_RW, group_ctrl);

    /* current sensor raw readings */
    alias_variable(&mc_session.hall_a_raw, "hall.a.raw", FLOAT32, VAR_RW, group_sensor);
    alias_variable(&mc_session.hall_b_raw, "hall.b.raw", FLOAT32, VAR_RW, group_sensor);

    /* current sensor coefficients */
    alias_variable(&mc_session.controller_configs.hall_a_coeff, "hall.a.coeff", FLOAT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.controller_configs.hall_b_coeff, "hall.b.coeff", FLOAT32, VAR_RW, group_ctrl);

    /*
     * PID controllers
     * ---------------
     *  */
    /* position controller */
    alias_variable(&mc_session.position_controller.p, "pid_pos.p", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.position_controller.i, "pid_pos.i", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.position_controller.d, "pid_pos.d", FLOAT32, VAR_RW, group_encoder);

    /* velocity controller */
    alias_variable(&mc_session.velocity_controller.p, "pid_vel.p", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.velocity_controller.i, "pid_vel.i", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.velocity_controller.d, "pid_vel.d", FLOAT32, VAR_RW, group_encoder);

    /* torque controller */
    alias_variable(&mc_session.torque_controller.p, "pid_torque.p", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.torque_controller.i, "pid_torque.i", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.torque_controller.d, "pid_torque.d", FLOAT32, VAR_RW, group_encoder);

    /* flux controller */
    alias_variable(&mc_session.mag_flux_controller.p, "pid_flux.p", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.mag_flux_controller.i, "pid_flux.i", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.mag_flux_controller.d, "pid_flux.d", FLOAT32, VAR_RW, group_encoder);

    /*
     * Kalman filter constants
     * -----------------------
     *  */
    alias_variable(&mc_session.angular_vel_kalman_filter.Q.m11, "klm.q.m11", FLOAT32, VAR_RW, group_filter);
    alias_variable(&mc_session.angular_vel_kalman_filter.Q.m22, "klm.q.m22", FLOAT32, VAR_RW, group_filter);
    alias_variable(&mc_session.angular_vel_kalman_filter.Q.m33, "klm.q.m33", FLOAT32, VAR_RW, group_filter);

    alias_variable(&mc_session.angular_vel_kalman_filter.R.m11, "klm.r.m11", FLOAT32, VAR_RW, group_filter);
    alias_variable(&mc_session.angular_vel_kalman_filter.R.m22, "klm.r.m22", FLOAT32, VAR_RW, group_filter);
    alias_variable(&mc_session.angular_vel_kalman_filter.R.m33, "klm.r.m33", FLOAT32, VAR_RW, group_filter);

    /*
     * Motion controller
     * -----------------
     *  */
    alias_variable(&mc_session.motion.normalized_time,
                                            "normalized_time",      FLOAT32, VAR_RO, group_motion);
    alias_variable(&motion_type,            "motion_type",          INT32,   VAR_RW, group_motion);
    alias_variable(&motion_pause_length,    "motion_pause_length",  INT32,   VAR_RW, group_motion);
    alias_variable(&motion_pause_count,     "motion_pause_count",   INT32,   VAR_RW, group_motion);

    /*
     * Variable scope
     * -----------------
     *  */
    alias_variable(&scope_session.var_scope_div, "scope_div", INT32, VAR_RW, group_scope);
    alias_variable(&scope_session.var_scope_triggered, "var_scope_triggered", INT32, VAR_RO, group_scope);
    alias_variable(&scope_session.var_scope_sample_len, "var_scope_sample_len", INT32, VAR_RO, group_scope);
    alias_variable(&scope_session.record_length, "var_record_length", INT32, VAR_RO, group_scope);
    alias_variable(&scope_session.record_columns, "var_record_columns", INT32, VAR_RO, group_scope);

    /*
     * Variable frequency drive
     * ------------------------
     *  */
    alias_variable(&mc_session.vfd.torque_ratio, "vfd_torque_ratio", FLOAT32, VAR_RW, group_vfd);
    alias_variable(&mc_session.vfd.electric_angle_inc, "vfd_electric_angle_inc", FLOAT32, VAR_RW, group_vfd);
    alias_variable(&mc_session.vfd.frequency, "vfd_frequency", FLOAT32, VAR_RW, group_vfd);

    /*
     * controller pulse input settings
     * -------------------------------
     *  */
    alias_variable(&mc_session.controller_configs.pulse_input_mode, "pulse_input_mode", INT32, VAR_RW, group_ctrl);
    alias_variable(&mc_session.controller_configs.pulse_step_amount, "pulse_step_amount", FLOAT32, VAR_RW, group_ctrl);

    /*
     * inertia ratio detect & tuning
     * -----------------------------
     *  */
    alias_variable(&mc_session.encoder_configs.inertia_rotor_cal_duration, "inertia_rotor_cal_duration", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.inertia_rotor_cal_torque, "inertia_rotor_cal_torque", FLOAT32, VAR_RW, group_encoder);
    alias_variable(&mc_session.encoder_configs.inertia_rotor_calibrated, "inertia_rotor_calibrated", FLOAT32, VAR_RW, group_encoder);

    alias_variable(&mc_session.fft_session.vibration_fft_update_count, "fft_update_count", INT32, VAR_RO, group_performance);
    alias_array(&mc_session.fft_session.vibration_fft_density_spectrum, 128, "fft_density_spectrum", ARRAY_FLT, VAR_RO, group_performance);

#if 0
    // ------------------------------------------
    // self tuner
    /*    session.pos_pid_tuner.error_pb            = 200.0f;
    session.pos_pid_tuner.error_change_pb   = 10.0f;

    session.pos_pid_tuner.kp_pb     = 3.0f;
    session.pos_pid_tuner.ki_pb     = 0.02f;
    session.pos_pid_tuner.kd_pb     = 0.0f;

    session.pos_pid_tuner.p_prime_max = 3.0f;
    session.pos_pid_tuner.i_prime_max = 0.1f;*/
    alias_variable(&mc_session.pos_pid_tuner.error_pb, "pos_sf_error_pb", FLOAT32, VAR_RW, group("ALL", NULL));
    alias_variable(&mc_session.pos_pid_tuner.error_change_pb, "pos_sf_derror_pb", FLOAT32, VAR_RW, group("ALL", NULL));
    alias_variable(&mc_session.pos_pid_tuner.kp_pb, "pos_sf_kp_pb", FLOAT32, VAR_RW, group("ALL", NULL));
    alias_variable(&mc_session.pos_pid_tuner.p_prime_max, "pos_sf_p'_max", FLOAT32, VAR_RW, group("ALL", NULL));
    alias_variable(&mc_session.pos_pid_tuner.p, "pos_sf_p", FLOAT32, VAR_RW, group("ALL", NULL));
#endif

    return 0;
}

int initialize_default_values(void)
{
    /* reset differentiators */
    differentiator_angular_init(&mc_session.pos_diff, PERIOD_REVOLUTION);
    differentiator_float_init(&mc_session.pos_error_diff);

    differentiator_float_init(&mc_session.acc_diff);
    differentiator_float_init(&mc_session.jerk_diff);
    differentiator_float_init(&mc_session.vel_error_diff);

    /* samplers initialize */
    average_sampler_init(&mc_session.ang_vel_sampler, ANG_VEL_RESAMPLE_AVG);
    average_sampler_init(&mc_session.torque_sampler, TORQUE_RESAMPLE_AVG);
    average_sampler_init(&mc_session.motor_current_sampler, CURRENT_RESAMPLE_AVG);
    average_sampler_init(&mc_session.vel_control_sampler, 2);

    average_sampler_init(&mc_session.velocity_sampler, 5);
    average_sampler_init(&mc_session.position_sampler, 10);

    average_sampler_init(&mc_session.fft_session.fft_sampler, 10);

    fft256_dma_start(mc_session.fft_session.vibration_fft_sample_buffer);

    iir_filter_reset(&mc_session.fft_session.notch_filter);
    mc_session.fft_session.notch_filter_enabled = 0;

    mc_session.fft_session.vibration_fft_current_sample = 0;
    mc_session.fft_session.vibration_fft_triggered = 0;

    for(int i = 0; i < VIBS_FFT_PTS; i++)
    {
        mc_session.fft_session.vibration_fft_sample_buffer[i] = 0.0f;
    }

    mc_session.fft_session.vibration_fft_state = FFT_STATE_SAMPLING;
    mc_session.fft_session.vibration_fft_update_count = 0;

    /* observers initialize */
    ab_observer_init(&mc_session.vel_ff_observer, 1.0f, 0.10f, 0.02f);
    ab_observer_init(&mc_session.torque_ff_observer, 1.0f, 0.55f, 0.02f);

    /* motion queue initialize */
    motion_queue_init(&motion_queue, MOTION_QUEUE_LENGTH);

    /* reset vibration suppression notch filters */
    for(int i = 0; i < 10; i++)
    {
        iir_filter_reset(&mc_session.notch_filters[i]);
    }

    mc_session.state = 4;

    /* motor settings
     * These variables should be initialized by loading from the EEPROM */
    mc_session.encoder_configs.encoder_offset       = 0;
    mc_session.encoder_configs.poles                = 0;
    mc_session.rotor_angular_vel_control            = 0;
    mc_session.free_run_torque                      = 0.0;
    mc_session.encoder_configs.rotor_vel_limit      = 30000.0f;
    mc_session.encoder_configs.torque_limit_ccw         = 40.0f;
    mc_session.encoder_configs.torque_feedforward   = 0.0f;

    mc_session.controller_configs.hall_a_bias       = 0;        /* P10:1 */
    mc_session.controller_configs.hall_b_bias       = 0;        /* P10:2 */
    mc_session.controller_configs.hall_a_coeff      = 0.0f;     /* P10:3 */
    mc_session.controller_configs.hall_b_coeff      = 0.0f;     /* P10:4 */

    mc_session.encoder_configs.torque_coeff         = 0.06f;
    mc_session.encoder_configs.vel_feedforward      = 0.0f;

    mc_session.flux_cmd_max                         = 0.95f;
    mc_session.flux_cmd_integral_max                = 40.0f;

    /* position control loop: velocity integral limit */
    mc_session.rot_ang_vel_integral_max             = 500.0f;

    /* digital I/O configs */
    mc_session.controller_configs.dio_config.dir    = 0;        /* P91:5 */
    mc_session.controller_configs.dio_config.usage  = 0;        /* P91:6 */
    mc_session.dio_state.dio_out                    = 0;
    mc_session.dio_state.dio_in                     = 0;

    mc_session.pulse_move_amount                    = 0;

    mc_session.controller_configs.out_of_control_thres = 1000;

    mc_session.encoder_check_enabled                = 0;

    /* clear process variables */
    mc_session.rot_pos_set                  = angular_pos_from_float(0.0f, PERIOD_REVOLUTION);
    mc_session.mechanical_angle_prev        = 0;
    mc_session.x_offset                     = angular_pos_from_float(0.0f, PERIOD_REVOLUTION);
    mc_session.encoder_misscode_count       = 0;
    mc_session.rot_pos_meas_prev            = angular_pos_from_float(0.0f, PERIOD_REVOLUTION);
    mc_session.led_tick_count               = 0;
    mc_session.tick                         = 0;
    mc_session.stop                         = 0;
    mc_session.encoder_count                = 0;

    /* reset performance counter */
    mc_session.performance.current          = 0;
    mc_session.performance.max              = 0;
    mc_session.performance.min              = INT32_MAX;
    mc_session.performance.pos_loop_tick    = 0;
    mc_session.performance.vel_loop_tick    = 0;

    /* velocity filter
     * TODO: should be designed on the fly */
    mc_session.vel_filter.a0 = 0.3;
    mc_session.vel_filter.a1 = -0.0;
    mc_session.vel_filter.a2 = 0.0;
    mc_session.vel_filter.b1 = 0.7;
    mc_session.vel_filter.b2 = -0.0;

    mc_session.vel_filter.x_1 = 0.0f;
    mc_session.vel_filter.x_2 = 0.0f;
    mc_session.vel_filter.y_1 = 0.0f;
    mc_session.vel_filter.y_2 = 0.0f;

    /* XXX: move to EEPROM */
    mc_session.enabled_iir_notch_filters = 0;

    // scope default values
    scope_session.var_scope_div             = 5;
    scope_session.var_scope_trigger_count   = 0;
    scope_session.var_scope_sample_len      = 0;
    scope_session.var_scope_triggered       = 0;

    // VFD initialize
    mc_session.vfd.electric_angle           = 0.0f;
    mc_session.vfd.electric_angle_inc       = 0.0f;
    mc_session.vfd.frequency                = 0.0f;
    mc_session.vfd.torque_ratio             = 0.0f;

    mc_session.set_angle                    = 0.0f;
    mc_session.load_inertia_ratio           = 1.0f;

    //////
    // XXX: move this to encoder EEPROM
    float cal_inertia_a = 1.0f;
    float cal_inertia_b = 10.0f;

    linear_init(&mc_session.auto_tunning.pos_p, cal_inertia_a, 7.0f, cal_inertia_b, 2.0f);
    linear_init(&mc_session.auto_tunning.vel_i, cal_inertia_a, 0.005f, cal_inertia_b, 0.04f);
    linear_init(&mc_session.auto_tunning.vel_p, cal_inertia_a, 0.000065f, cal_inertia_b, 0.00032f);
    linear_init(&mc_session.auto_tunning.torque_ff, cal_inertia_a, 4.0f, cal_inertia_b, 40.0f);
    //////

    ////////////////////////////////////////////////////////////////
    // self tuner
    mc_session.pos_pid_tuner.error_pb           = 40.0f;
    mc_session.pos_pid_tuner.error_change_pb    = 10.0f;

    mc_session.pos_pid_tuner.kp_pb      = 5.0f;
    mc_session.pos_pid_tuner.ki_pb  = 0.02f;
    mc_session.pos_pid_tuner.kd_pb  = 0.0f;

    mc_session.pos_pid_tuner.p_prime_max = 5.0f;
    mc_session.pos_pid_tuner.i_prime_max = 0.1f;

    // vel self tuner
    mc_session.vel_pid_tuner.error_pb = 200.0f;
    mc_session.vel_pid_tuner.error_change_pb = 20.0f;

    mc_session.vel_pid_tuner.kp_pb = 0.0f;
    mc_session.vel_pid_tuner.ki_pb = 0.000005f;

    mc_session.vel_pid_tuner.p_prime_max = 0.0f;
    mc_session.vel_pid_tuner.i_prime_max = 0.0005f;
    ////////////////////////////////////////////////////////////////

    mc_session.encoder_info.friction_comp_rpm_h = 0.0f;
    mc_session.encoder_info.friction_comp_rpm_l = 0.0f;
    mc_session.encoder_info.friction_comp_torque_h = 0.0f;
    mc_session.encoder_info.friction_comp_torque_l = 0.0f;

    lprintf("default values set\r\n");

    return 0;
}

void kalman_filter_setup(motor_control_session_t *session)
{
    float dt = 0.5;

    /* prediction matrix */
    INSMatrix33f A = {
        .m11 = 1.0, .m12 = -(dt * dt) / 2.0,    .m13 = 0.0,
        .m21 = 0.0, .m22 = 1.0,                 .m23 = -(dt * dt) / 2.0,
        .m31 = 0.0, .m32 = 0.0,                 .m33 = 1.0
    };

    /* covariance of the error noise */
    INSMatrix33f Q = {
        0.05, 0.0, 0.0,
        0.0, 0.0005, 0.0,
        0.0, 0.0, 0.00001
    };

    /* noise in the sensor measurement */
    INSMatrix33f R = {
        800.0, 0.0, 0.0,
        0.0, 1000.0, 0.0,
        0.0, 0.0, 2000.0
    };

    /* measurement matrix */
    INSMatrix33f H = {
        .m11 = 1.0, .m12 = 0.0, .m13 = 0.0,
        .m21 = 0.0, .m22 = 1.0, .m23 = 0.0,
        .m31 = 0.0, .m32 = 0.0, .m33 = 1.0
    };

    /* control matrix */
    INSMatrix33f B = M33F_IDENTITY;

    insKalmanFilterInit(&session->angular_vel_kalman_filter, A, Q, R, B, H);

    lprintf("Kalman filter: matrices loaded\r\n");
}

int initialize_filters(void)
{
    median_filter_init(&mc_session.current_filter_a, 5);
    median_filter_init(&mc_session.current_filter_b, 5);


    float a[] = { 1.0f, -1.1429805025399, 0.4128015980 };
    float b[] = { 1.0f, 2.0f, 1.0f };
    iir_filter_nth_init(&mc_session.vel_command_filter, a, b, 2);

    return 0;
}

void motor_control_initialize_components(void)
{
    for(int i = 0; i < 100; i++)
    {
        motor_control_read_encoder(mc_session.encoder_configs.direction);
    }

    mc_session.state = CONTROL_VELOCITY;
    mc_session.encoder_count = motor_control_read_encoder(mc_session.encoder_configs.direction);
    mc_session.rot_pos_set = angular_pos_from_float(motor_control_read_encoder(mc_session.encoder_configs.direction), PERIOD_REVOLUTION);
    mc_session.mechanical_angle_prev = angular_pos_to_float(mc_session.rot_pos_set, PERIOD_REVOLUTION);
    mc_session.rot_pos_meas = mc_session.rot_pos_set;
    mc_session.rot_vel_meas = 0;
    mc_session.rot_vel_set = 0;

    for(int i = 0; i < 1000; i++)
    {
        ab_observer_update(&mc_session.vel_ff_observer, (float)angular_pos_to_float(mc_session.rot_pos_set, PERIOD_REVOLUTION));
        ab_observer_update(&mc_session.torque_ff_observer, mc_session.vel_ff_observer.vk_1);

        mc_session.rot_vel_meas = iir_filter_update(&mc_session.vel_filter, mc_session.rot_vel_meas);
    }

    for(int i = 0; i < 1000; i++)
    {
        motor_control_read_sensors(&mc_session);
    }

    mc_session.encoder_misscode_count = 0;
    mc_session.encoder_check_enabled = 1;

    lprintf("components initial values set\r\n");
}
