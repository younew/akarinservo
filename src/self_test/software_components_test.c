/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "self_test.h"
#include "../angular_pos.h"

#define ASSERT_FLOAT(x, y, err) \
    if(fabs((x) - (y)) > 0.0001) err = 1

int differentiator_angular_t_test(void)
{
    int error = 0;

    differentiator_angular_t diff;
    differentiator_angular_init(&diff, 52.0f);

    for(int i = 0; i < 100; i++)
    {
        angular_pos_t ang = angular_pos_from_float(i, 52.0f);

        float d = differential_angular_pos(&diff, ang);
        if(i > 5 && fabs(1.0f - d) >= 0.0001)
        {
            error = 1;
            break;
        }
    }

    for(int i = 100; i < 0; i--)
    {
        angular_pos_t ang = angular_pos_from_float(i, 52.0f);

        float d = differential_angular_pos(&diff, ang);
        if(i > 5 && fabs(d + 1.0f) >= 0.0001)
        {
            error = 1;
            break;
        }
    }

    return error;
}

int angular_pos_test(void)
{
    angular_pos_t ang1, ang2;

    int error = 0;

#define TEST_PERIOD 16384.0f

    /* conversion test */
    ang1 = angular_pos_from_float(-2000.0f, TEST_PERIOD);
    ASSERT_FLOAT(angular_pos_to_float(ang1, TEST_PERIOD), -2000.0f, error);

    ang1 = angular_pos_from_float(2000.0f, TEST_PERIOD);
    ASSERT_FLOAT(angular_pos_to_float(ang1, TEST_PERIOD), 2000.0f, error);

    ang1 = angular_pos_from_float(102000.0f, TEST_PERIOD);
    ASSERT_FLOAT(angular_pos_to_float(ang1, TEST_PERIOD), 102000.0f, error);

    ang1 = angular_pos_from_float(-102000.0f, TEST_PERIOD);
    ASSERT_FLOAT(angular_pos_to_float(ang1, TEST_PERIOD), -102000.0f, error);

    /* operation plus */
    ang1 = angular_pos_from_float(-2000.0f, TEST_PERIOD);
    ang1 = angular_pos_plus_pos(ang1, 100, TEST_PERIOD);
    ASSERT_FLOAT(angular_pos_to_float(ang1, TEST_PERIOD), -1900.0f, error);

    /* operation minus */
    ang1 = angular_pos_from_float(-102000.0f, TEST_PERIOD);
    ang2 = angular_pos_from_float(102000.0f, TEST_PERIOD);
    ASSERT_FLOAT(angular_pos_to_float(angular_pos_plus(ang1, ang2, TEST_PERIOD), TEST_PERIOD), 0.0f, error);

    ang1 = angular_pos_from_float(102000.0f, TEST_PERIOD);
    ang2 = angular_pos_from_float(102000.0f, TEST_PERIOD);
    ASSERT_FLOAT(angular_pos_to_float(angular_pos_difference(ang1, ang2, TEST_PERIOD), TEST_PERIOD), 0.0f, error);

    return error;
}

int software_components_post(void)
{
	if(differentiator_angular_t_test() != 0)
	{
		printf("post fail: differentiator_angular_t\n");
		return 1;
	}

	if(angular_pos_test() != 0)
	{
		printf("post fail: angular_pos\n");
		return 1;
	}

	return 0;
}
