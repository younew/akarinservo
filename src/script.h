/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCRIPT_H_
#define SCRIPT_H_

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SCRIPT_LEN  		1024
#define SCRIPT_NAME_LEN 	16
#define SCRIPT_STORAGE_LEN	1024

typedef struct script_t
{
    char *name;
    char *text;
    uint32_t text_len;
    uint32_t name_len;

    struct script_t *next;
} script_t;

typedef struct
{
    script_t *script;
} scripts_list_t;

extern uint32_t scripts_storage[SCRIPT_STORAGE_LEN];
extern scripts_list_t script_list;

extern uint32_t script_malloc_count;

static inline void *script_malloc(size_t size)
{
	void *ptr = malloc(size);

	if(ptr)
	{
		script_malloc_count++;
	}

	return ptr;
}

static inline void *script_realloc(void *ptr, size_t size)
{
	void *new_addr = realloc(ptr, size);
	return new_addr;
}

static inline void script_free(void *ptr)
{
	if(ptr)
	{
		free(ptr);
		script_malloc_count--;
	}
}

typedef int(*script_line_func_t)(char *);

void script_list_delete(scripts_list_t *list);

script_t *script_new(char *name, char *text);

script_t *script_delete(scripts_list_t *list, char *name);

script_t *script_add(scripts_list_t *list, char *name, char *text);

scripts_list_t *script_save(scripts_list_t *list, uint32_t *storage);

scripts_list_t *script_load(scripts_list_t *list, uint32_t *storage, uint32_t len);

script_t *script_find(scripts_list_t *list, char *name, script_t **prev_script);

script_t *script_append(script_t *script, char *line);

script_t *script_rename(script_t *script, char *name);

script_t *script_clear(script_t *script);

#endif /* SCRIPT_H_ */
