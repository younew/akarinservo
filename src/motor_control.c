/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>

#include "kalman/kalman_filter.h"
#include "command/command.h"
#include "kinematics/delta_kinematics.h"
#include "utils/log.h"

int echo_enable = 0;
int debug_enable = 0;

StringHashMap *command_list;
StringHashMap *variable_map;

motor_control_session_t mc_session;
scope_session_t 		scope_session;
motion_queue_t 			motion_queue;

float home_vel_max = 5.0f;
float home_vel = 0.0f;
float home_acc = 0.001f;
float home_decel = 0.01f;
int64_t home_click = 0;
int home_found = 0;

KVMap *controller_kvmap;
KVMap *encoder_kvmap;

void motor_control_modulate(float vd, float vq, float sint, float cost)
{
    /* Inverse Park Transformation */
    const float beta  = vd * sint + vq * cost;
    const float alpha = vd * cost - vq * sint;

    /* run SVPWM algorithm */
    float aon, bon, con;
    svpwm(mc_pwm_period, alpha, beta, &aon, &bon, &con);

    /* update PWM comparator, we use integer thresholds */
    update_pwm_comp((int)aon, (int)bon, (int)con);
}

static int fuzzy_logic_rule_kp[] = {
    PB, PB, PM, PM, PS, ZO, ZO,
    PB, PB, PM, PS, PS, ZO, NS,
    PM, PM, PM, PS, ZO, NS, NS,
    PM, PM, PS, ZO, NS, NM, NM,
    PS, PS, ZO, NS, NS, NM, NM,
    PS, ZO, NS, NM, NM, NB, NB,
    ZO, ZO, NM, NM, NM, NB, NB
};

static int fuzzy_logic_rule_ki[] = {
    NB, NB, NM, NM, NS, ZO, ZO,
    NB, NB, NM, NS, NS, ZO, ZO,
    NB, NM, NS, NS, ZO, PS, PS,
    NM, NM, NS, ZO, PS, PM, PM,
    NM, NS, ZO, PS, PS, PM, PB,
    ZO, ZO, PS, PS, PM, PB, PB,
    ZO, ZO, PS, PM, PM, PB, PB
};

static int fuzzy_logic_rule_kd[] = {
    PS, NS, NB, NB, NB, NM, PS,
    PS, NS, NM, NM, NM, NS, ZO,
    ZO, NS, NM, NM, NS, NS, ZO,
    ZO, NS, NS, NS, ZO, NS, ZO,
    ZO, ZO, ZO, ZO, PS, ZO, ZO,
    PB, PS, PS, PS, PS, PS, PB,
    PB, PM, PM, PM, NS, PS, PB
};

void motor_control_self_tuning(
        pid_fuzzy_self_tuner *tuner,
        pid_controller_t *pid,
        float error, float derror
) {
    /* now calculate fuzzy logic level of error and change of error
     * using the membership function */
    tuner->error_level =
            fuzzy_logic_membership(0, tuner->error_pb, fabs(error));
    tuner->error_change_level =
            fuzzy_logic_membership(-tuner->error_change_pb, tuner->error_change_pb, derror);

    /* find the fuzzy rule using the calculated error level and error change level */
    tuner->kp_level = fuzzy_logic_find(tuner->error_level, tuner->error_change_level, fuzzy_logic_rule_kp);
    tuner->ki_level = fuzzy_logic_find(tuner->error_level, tuner->error_change_level, fuzzy_logic_rule_ki);
    tuner->kd_level = fuzzy_logic_find(tuner->error_level, tuner->error_change_level, fuzzy_logic_rule_kd);

    /* defuzzify output logic, we get pk_prime, which is the correction term */
    tuner->kp_prime = fuzzy_logic_defuzzify(-tuner->kp_pb, tuner->kp_pb, tuner->kp_level);
    tuner->ki_prime = fuzzy_logic_defuzzify(-tuner->ki_pb, tuner->ki_pb, tuner->ki_level);
    tuner->kd_prime = fuzzy_logic_defuzzify(-tuner->kd_pb, tuner->kd_pb, tuner->kd_level);

    pid->p_prime -= tuner->kp_prime;
    pid->i_prime += tuner->ki_prime;

    if(pid->p_prime > tuner->p_prime_max) pid->p_prime = tuner->p_prime_max;
    if(pid->p_prime < -tuner->p_prime_max) pid->p_prime = -tuner->p_prime_max;

    if(pid->i_prime > tuner->i_prime_max) pid->i_prime = tuner->i_prime_max;
    if(pid->i_prime < -tuner->i_prime_max) pid->i_prime = -tuner->i_prime_max;

    //pid->p_prime = iir_filter_update(&session.tuner_filter, pid->p_prime);

    tuner->p = pid->p + pid->p_prime;
}

int motion_type = 0;
int motion_pause_length = 0;
int motion_pause_count = 0;
int motion_trigger = 0;

void variable_scope_tick(motor_control_session_t *session, scope_session_t *scope)
{
	int new_sample_flag = 0;
	float sampler_update_value[5];

	/* return if not in sampling state */
	if(!(scope->var_scope_sample_len < scope->var_scope_trigger_count && !scope->var_scope_triggered))
	{
		return;
	}

	/* update average samplers */
	for(int j = 0; j < scope_session.record_columns; j++)
	{
		sampler_update_value[j] =
				average_sampler_update(&scope_session.scope_samplers[j],
						extract_whatever(scope->var_scope_var[j]),
						&new_sample_flag
				);
	}

	/* new sample available */
	if(new_sample_flag)
    {
		int index = scope->var_scope_sample_len++;

		/* copy average sampler readings */
		for(int i = 0; i < scope_session.record_columns; i++)
		{
			scope->var_scope_buffer[i][index] = sampler_update_value[i];
		}

		/* we have enough samples */
		if(scope->var_scope_sample_len == scope->var_scope_trigger_count - 1)
		{
			scope->var_scope_triggered = 1;

			scope->var_scope_sample_len = 0;
			scope->var_scope_trigger_count = 0;
		}
    }
}

static inline float gear(motor_control_session_t *session, float x)
{
	reference_config_t *cfg = &session->ref_config;
	return x * cfg->elec_gear_n / cfg->elec_gear_m * session->encoder_info.rev_pulse;
}

static inline void motion_set_output(motor_control_session_t *session, float pos_demand)
{
	angular_pos_t output = angular_pos_from_float(
			gear(session, pos_demand),
			PERIOD_REVOLUTION);

	session->rot_pos_set = angular_pos_difference(output, session->x_offset, PERIOD_REVOLUTION);
}

motion_t current_motion;
bezier3d_3rd_interpolator_t b3d;

void motion_control_execute(motor_control_session_t *session)
{
    if(session->state == CONTROL_POSITION &&
       !motion_queue_empty(&motion_queue) &&
       motion_trigger > 0)
    {
        /* dequeue a motion from the queue and execute it */
        motion_dequeue(&motion_queue, &current_motion);

        /* all motion sequence extracted, reset the trigger */
        if(motion_queue_empty(&motion_queue))
        {
            motion_trigger = 0;
        }

        motion_type = current_motion.motion_type;

        reference_config_t *cfg = &session->ref_config;
        float current_count = angular_pos_to_float(angular_pos_plus(session->rot_pos_set, session->x_offset, PERIOD_REVOLUTION),
				PERIOD_REVOLUTION);
        float start_mm = current_count / PERIOD_REVOLUTION * cfg->elec_gear_m / cfg->elec_gear_n;

        if(motion_type == MOTION_CUSTOM || motion_type == MOTION_B3D)
        {
        	start_mm = 0;
        	b3d.pts[0] = current_motion.custom_motion_start;
        	b3d.pts[1] = current_motion.custom_b3d_p1;
        	b3d.pts[2] = current_motion.custom_b3d_p2;
        	b3d.pts[3] = current_motion.custom_motion_end;

        	lprintf("%f %f %f\n", b3d.pts[0].x, b3d.pts[0].y, b3d.pts[0].z);
        }

		/* setup motion controller */
		motion_prepare(&session->motion,
				start_mm,
			current_motion.target, current_motion.target_acc, current_motion.target_vel, 5e-05);


        motion_pause_length = current_motion.duration;

        /* now off we go */
        session->state = CONTROL_MOTION;
    }

    if(session->state == CONTROL_MOTION)
    {
        switch(motion_type)
        {
        	case MOTION_B3D:
			{
				if(motion_control_tick(&session->motion))
				{
					INSVector3f result = bezier3d_3rd(&b3d, session->motion.normalized_time);

					float t1, t2, t3;
					delta_inverse_kinematics(result.x, result.y, result.z + delta_top_z(), &t1, &t2, &t3);

					delta_current_position = result;

					/* set output according to axis */
					switch(current_motion.custom_motion_axis)
					{
						case AXIS_A: motion_set_output(session, t1); break;
						case AXIS_B: motion_set_output(session, t2); break;
						case AXIS_C: motion_set_output(session, t3); break;

						default:
							break;
					}
				}
				else
				{
					session->state = CONTROL_POSITION;
					LED_BLUE_HIGH();
				}
				break;
			}

        	case MOTION_CUSTOM:
        	{
        		if(motion_control_tick(&session->motion))
        		{
            		INSVector3f result =
            		interpolate_3d_linear(current_motion.custom_motion_start, current_motion.custom_motion_end,
            				session->motion.normalized_time
            		);

            		float t1, t2, t3;
            		delta_inverse_kinematics(result.x, result.y, result.z + delta_top_z(), &t1, &t2, &t3);

            		delta_current_position = result;

            		/* set output accroding to axis */
            		switch(current_motion.custom_motion_axis)
            		{
            			case AXIS_A: motion_set_output(session, t1); break;
            			case AXIS_B: motion_set_output(session, t2); break;
            			case AXIS_C: motion_set_output(session, t3); break;

            			default:
            				break;
            		}
        		}
        		else
        		{
                    session->state = CONTROL_POSITION;
                    LED_BLUE_HIGH();
        		}

        		break;
        	}

            case MOTION_GO:
            {
                /* run motion controller */
                if(motion_control_tick(&session->motion))
                {
                    /* set the rotor position to the position demanded by
                     * the motion controller */
                	motion_set_output(session, session->motion.pos_output);

                    LED_BLUE_LOW();
                }
                else
                {
                    /* motion control ends, go back to CONTROL_RUN mode */
                    session->state = CONTROL_POSITION;

                    LED_BLUE_HIGH();
                }
            }
            break;

            case MOTION_PAUSE:
            {
                motion_pause_count++;

                if(motion_pause_count == motion_pause_length)
                {
                    motion_pause_count = 0;
                    /* motion control ends, go back to CONTROL_RUN mode */
                    session->state = CONTROL_POSITION;

                    LED_BLUE_HIGH();
                }
            }
            break;

            case MOTION_G1_HIGH:
            {
            	G1_OUT_HIGH();
            	session->state = CONTROL_POSITION;
            }
            break;

            case MOTION_G1_LOW:
            {
            	G1_OUT_LOW();
            	session->state = CONTROL_POSITION;
            }
            break;

            case MOTION_ZERO:
            {
            	execute_command(&mc_session, command_list, "zero");

            	session->state = CONTROL_POSITION;
            	break;
            }

            case MOTION_HOME:
            {
            	if(mc_session.dio_home_switch == 0)
            	{
            		home_vel += home_acc;
            		if(home_vel >= home_vel_max)
            		{
            			home_vel = home_vel_max;
            		}

            		session->rot_pos_set = angular_pos_plus_pos(session->rot_pos_set, home_vel, PERIOD_REVOLUTION);
            	}
            	else
            	{
             		if(home_found == 0)
                	{
                		home_click = angular_pos_to_float(session->rot_pos_set, PERIOD_REVOLUTION);
               			home_found = 1;
               		}

            		home_vel -= home_decel;
            		session->rot_pos_set = angular_pos_plus_pos(session->rot_pos_set, home_vel, PERIOD_REVOLUTION);
            		if(home_vel <= 0.0f)
            		{
            			session->x_offset = angular_pos_from_float(-home_click, PERIOD_REVOLUTION);

            			session->state = CONTROL_POSITION;
            		}
            	}

            }
            break;

            default:
            break;
        }


    }
}

int os_count = 0;

int64_t motor_control_encoder_position_over_sample(int64_t encoder_count, int *flag)
{
	os_count++;
	if(os_count == 4)
	{
		os_count = 0;
		*flag = 1;
		return encoder_count;
	}

	/* we don't have enough samples yet, do nothing */
	return encoder_count;
}

void motor_control_read_sensors(motor_control_session_t *session)
{
    /* activate ADC conversion */
    ADC_CONVST_LOW();

    /* read encoder angle once the ADC conversion starts */
    session->encoder_raw = motor_control_read_encoder(session->encoder_configs.direction);
    ADC_CONVST_HIGH();

    adc_read_data(&session->hall_a_raw, &session->hall_b_raw);

    session->hall_a_raw = median_filter_update(&session->current_filter_a, session->hall_a_raw);
    session->hall_b_raw = median_filter_update(&session->current_filter_b, session->hall_b_raw);

    /* current sensor calibration constants */
    session->hall_a_raw += session->controller_configs.hall_a_bias;
    session->hall_b_raw += session->controller_configs.hall_b_bias;

    /* current sensor B is installed in the opposite direction */
    session->hall_b_raw = -session->hall_b_raw;

    /* convert readings to Ampere */
    session->current_a = session->hall_a_raw * session->controller_configs.hall_a_coeff;
    session->current_b = session->hall_b_raw * session->controller_configs.hall_b_coeff;

//#define __SIMULATE_ENCODER_MISSCODE

#ifdef __SIMULATE_ENCODER_MISSCODE
    if(rand() % 100 == 0)
    {
    	encoder_mechanical_angle = 16384;
    }
#endif

    /* here we calculate the angular 'distance' instead of rotor angle.
     * we first take the differential of the rotor angle and perform a
     * periodic transform, then calculate its integral again. */
    int encoder_delta = (session->encoder_raw - session->mechanical_angle_prev);

    /* periodic transform */
    if(encoder_delta >  session->encoder_info.half_rev_pulse) encoder_delta -= session->encoder_info.rev_pulse;
    if(encoder_delta < -session->encoder_info.half_rev_pulse) encoder_delta += session->encoder_info.rev_pulse;

    /* add measured delta to encoder count */
    session->encoder_count += encoder_delta;

    const angular_pos_t rot_pos_meas_new =
    		angular_pos_from_float(
    				session->encoder_count * session->encoder_info.encoder_ratio,
					PERIOD_REVOLUTION
			);

    /* encoder misscode detection:
     * considered misscode if the difference between previous sampled encoder position
     * and current encoder position is greater than ENCODER_MISSCODE_CHECK_THRES.
     * ignore current sample if encoder misscode did occur. */
	if(session->encoder_check_enabled &&
	   (ABS(angular_pos_difference_float(rot_pos_meas_new, session->rot_pos_meas_prev, PERIOD_REVOLUTION)) >
	    ENCODER_MISSCODE_CHECK_THRES))
	{
		/* misscode */
		session->encoder_misscode_count++;
	}
	else
	{
		session->rot_pos_meas = rot_pos_meas_new;
		session->rot_pos_meas_prev = session->rot_pos_meas;
		session->mechanical_angle_prev = session->encoder_raw;
	}

    /* calculate angular velocity, angular acceleration and
     * angular jerk using 3 differentiators */
    float ang_vel_raw = differential_angular_pos(&session->pos_diff, session->rot_pos_meas) * CONTROL_FREQUENCY;
    float ang_acc_raw = differential_float(&session->acc_diff, ang_vel_raw);
    float ang_jerk_raw = differential_float(&session->jerk_diff, ang_acc_raw);

    /* estimate the angular velocity using a Kalman filter.
     * unit is revolution per second */
    session->angular_state = insKalmanFilterUpdate(
        &session->angular_vel_kalman_filter,
        insVector3fMake(ang_vel_raw, ang_acc_raw, ang_jerk_raw),
        VECTOR3F_ZERO
    );

    /* calculate angular velocity in degrees per second
     * x is angular velocity,
     * y is angular acceleration,
     * z is angular jerk. */
    session->rot_vel_meas = session->angular_state.x * 0.02197265625f;//session->encoder_info.deg_per_pulse;
    session->rot_vel_meas = average_sampler_update(&session->velocity_sampler, session->rot_vel_meas, NULL);
    //session->rot_vel_meas = iir_filter_update(&session->vel_filter, session->rot_vel_meas);

    /* calculate mechanical angle range [0.0, 360.0) */
    const float rotor_mechanical_angle = session->encoder_raw * session->encoder_info.deg_per_pulse;
    session->electric_angle =
    		(rotor_mechanical_angle * session->encoder_configs.poles) +
			session->encoder_configs.encoder_offset;

    /* normalize electric angle to [0.0, 360.0) */
    session->electric_angle = angle_normalize_deg(session->electric_angle * session->encoder_configs.direction);
}

void motor_control_execute(motor_control_session_t *session)
{
    if(session->state == CONTROL_VFD)
    {
	    /* skip the control loop if session is stopped */
	    if(session->stop)
	    {
	        update_pwm_comp(0, 0, 0);
	        goto control_loop_break;
	    }

	    session->led_tick_count++;
	    session->tick++;

	    session->vfd.electric_angle_inc = session->vfd.frequency * FREQ_INC_RATIO;
    	session->vfd.electric_angle += session->vfd.electric_angle_inc;

    	if(session->vfd.electric_angle >= M_2PI) session->vfd.electric_angle -= M_2PI;
    	if(session->vfd.electric_angle <  0.0f)	 session->vfd.electric_angle += M_2PI;

    	motor_control_modulate(0.0f,
    			session->vfd.torque_ratio,
    			sinf(session->vfd.electric_angle),
				cosf(session->vfd.electric_angle)
		);
    }
    else
    {
    	motor_control_read_sensors(session);

	    /* electric angle in rad, this is used in the following calculations */
	    const float angle_rad = RAD(session->electric_angle);
	    const float cost = cosf(angle_rad);
	    const float sint = sinf(angle_rad);

	    /* Clarke transformation
	     * current 3->2 phase transform */
	    const float i_alpha = session->current_a;
	    const float i_beta = (session->current_a + 2.0 * session->current_b) / 1.7320508075688772;

	    /* Park transformation
	     * The two-axis orthogonal stationary reference frame quantities are
	     * transformed into rotating reference frame quantities */
	    /* Torque is in Newton-Meter */
	    session->current_meas = -i_alpha * sint + i_beta * cost;

	    /* calculate magnetic flux and torque */
	    session->flux_meas   = i_alpha * cost + i_beta * sint;
	    session->torque_meas = session->current_meas * session->encoder_configs.torque_coeff;

	    session->current_meas_filtered =
	    		average_sampler_update(&session->motor_current_sampler, session->current_meas, NULL);

	    /* skip the control loop if session is stopped */
	    if(session->stop)
	    {
	        update_pwm_comp(0, 0, 0);
	        goto control_loop_break;
	    }

        /* since we are using synchronous motor here, the magnetic flux
         * of the rotor should always be zero to achieve maximum efficiency.
         * before we start calculating any other control loops, the magnetic
         * flux control loop is calculated. */

        /* -------------------------
         * PID control loop
         * MAGNETIC FLUX LOOP
         * -------------------------
         * this keeps the magnetic flux id to zero */
        session->flux_cmd = pi_update(
                &session->mag_flux_controller,
                -session->flux_meas,            /* measured term */
                CONTROL_LOOP_TICK_TIME,         /* loop period */

                /* output saturation range */
               -session->flux_cmd_max, session->flux_cmd_max,

                /* integral saturation range */
               -session->flux_cmd_integral_max, session->flux_cmd_integral_max,
                0.0f                            /* feed forward term */
        );

    	motor_control_servo_loop(session, sint, cost, angle_rad);
    }


    /* torque protect detection */
    /*if(abs(session->torque_meas) > 1.0f)
    {
    	session->stop = 1;
    	SET_ERROR(ERROR_TORQUE_LIM_PROCTECT);
    }
    else
    {
    	CLR_ERROR(ERROR_TORQUE_LIM_PROCTECT);
    }*/

    if(abs(session->current_meas_filtered) > session->encoder_configs.motor_current_protect_limit)
    {
    	CONTROL_STOP(session);
    	SET_ERROR(ERROR_OVER_CURRENT);
    }
    else
    {
    	CLR_ERROR(ERROR_OVER_CURRENT);
    }

control_loop_break:

	variable_scope_tick(session, &scope_session);

    return;
}
