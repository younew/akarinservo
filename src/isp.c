/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "isp.h"
#include "hw/sharc_hw.h"

int parse_hex_uint8(char *hex, int start)
{
    int len = (int)strlen(hex);

    /* out of bound */
    if(start + 1 > len)
    {
        return -1;
    }

    char hexstring[] = { hex[start], hex[start + 1] };
    int number = (int)strtol(hexstring, NULL, 16);
    return number;
}

int parse_hex_uint16(char *hex, int start)
{
    int len = (int)strlen(hex);

    /* out of bound */
    if(start + 3 > len)
    {
        return -1;
    }

    char hexstring[] = { hex[start], hex[start + 1], hex[start + 2], hex[start +3] };
    int number = (int)strtol(hexstring, NULL, 16);
    return number;
}

int parse_ihex32(char *str, unsigned short *buf, int *addr, int *type, int *cc_error)
{
    int len = (int)strlen(str);

    if(len == 0)
    {
        return -1;
    }

    if(str[0] != ':')
    {
        /* an Intel Hex-32 record starts with ':' */
        return -1;
    }

    int record_length = parse_hex_uint8(str, 1);
    int start_offset = parse_hex_uint16(str, 3);
    int record_type = parse_hex_uint8(str, 7);
    int cc = parse_hex_uint8(str, len - 2);

    /* verify record length */
    if(record_length * 2 + 11 != len)
    {
        printf("length incorrect\n");
        return -1;
    }

    uint32_t cksm = 0;
    for(int i = 1; i < 9; i += 2)
    {
        cksm += parse_hex_uint8(str, i);
    }

    int count = 0;
    for(int i = 9; i < len - 2; i += 2)
    {
        buf[count++] = parse_hex_uint8(str, i);
        cksm += buf[count - 1];
    }

    cksm =~ cksm;

    if(((cksm + 1) & 0xff) != cc)
    {
    	*cc_error = 1;
    }

    *addr = start_offset;
    *type = record_type;

    return record_length;
}

#include "hw/m25p16/flash.h"
#include "hw/m25p16/m25p16.h"

#define MAN_CODE		0x20		/* Numonyx */
#define DEV_CODE		0x15		/* M25P16 (16Mbit SPI flash) */
#define NUM_SECTORS		32			/* sectors */

ERROR_CODE dsp_spi_m25p16_setup(void)
{
	*pPWMGCTL = 0x00;
	*pPWMCTL3 = 0x00;
	*pPWMCTL2 = 0x00;
	*pPWMCTL1 = 0x00;
	*pSPICTL = 0x00;
	*pSYSCTL &= ~(0x4e000000);

	// encoder cs, fram cs
	SRU(HIGH, DAI_PBEN10_I);
	SRU(HIGH, DAI_PB10_I);
	SRU(HIGH, DAI_PBEN08_I);
	SRU(HIGH, DAI_PB08_I);
	// ctrl fram cs
	SRU(HIGH, DAI_PBEN20_I);
	SRU(HIGH, DAI_PB20_I);
	// ADC cs
	SRU(HIGH, DAI_PBEN14_I);
	SRU(HIGH, DAI_PB14_I);

	SRU(DPI_PB02_O, SPI_MISO_I);
	SRU(LOW, DPI_PBEN02_I);

	SRU(SPI_MOSI_O, DPI_PB01_I);
	SRU(HIGH, DPI_PBEN01_I);

	for(int i = 0; i < 1000; i++)
	{
		asm("nop;");
	}

	SRU(SPI_CLK_O,DPI_PB03_I);
	SRU(HIGH,DPI_PBEN03_I);

	// for the flag pins to act as chip select
	SRU(FLAG4_O, DPI_PB05_I);
	SRU(HIGH, DPI_PBEN05_I);

	//First set flag 4 as an output
	sysreg_bit_set( sysreg_FLAGS, FLG4O );
	sysreg_bit_set( sysreg_FLAGS, FLG4 );

	*pSPIDMAC = 0;
	*pSPIBAUD = 0;
	*pSPIFLG = 0xF80;
	*pSPICTL = 0x400;

	return NO_ERR;
}

void m25p16_test(void)
{
	int i, j;								/* indexes */
		unsigned int iPassed = 0;				/* pass flag */
		ERROR_CODE Result = NO_ERR;				/* result */
		COMMAND_STRUCT pCmdBuffer;				/* command buffer */
		unsigned short usRd = 0, usWr = 0;		/* storage for data */
		int nManCode = 0, nDevCode = 0;			/* man and device ids */
		static unsigned int uiSectors = 0;		/* we use different sectors each time */

	/* open the SPI flash */
	Result = m25p16_Open();

	if (Result != NO_ERR)
	{
		uprintf("Could not open flash driver\r\n");
		return;
	}

	/* get the codes */
	pCmdBuffer.SGetCodes.pManCode = (unsigned long *)&nManCode;
	pCmdBuffer.SGetCodes.pDevCode = (unsigned long *)&nDevCode;
	pCmdBuffer.SGetCodes.ulFlashStartAddr = 0x0;
	Result = m25p16_Control(CNTRL_GET_CODES, &pCmdBuffer);

	uprintf("manufacturer code: 0x%x, device code: 0x%x\r\n", nManCode, nDevCode);

	/* if codes don't match what we expect then we should fail */
	if ( (MAN_CODE != nManCode) || (DEV_CODE != nDevCode) )
	{
		uprintf("Flash codes do not match what we expected\r\n" );;
		return;
	}
	else
	{
		uprintf("flash detected\r\n");
	}
}

int base_addr = 0;

int flash_cmd(int argc, char *argv[])
{
	if(argc != 1 && argc != 2 && argc != 3)
	{
		uputs("usage: flash <enable | erase | write> [record | start] [len]\r\n");
		return 1;
	}

	if(argc == 1 && strcmp(argv[0], "enable") == 0)
	{
		uputs("enter flash programming mode\r\n");

	    dsp_spi_m25p16_setup();
	    m25p16_test();

		return 0;
	}

	if(argc == 1 && strcmp(argv[0], "erase") == 0)
	{
		uputs("erase flash");

		COMMAND_STRUCT pCmdBuffer;
		pCmdBuffer.SEraseAll.ulFlashStartAddr = 0;
		m25p16_Control(CNTRL_ERASE_ALL, &pCmdBuffer);

		return 0;
	}

	if(argc == 2 && strcmp(argv[0], "erase") == 0)
	{
		/* erase block */
		ERROR_CODE ErrorCode = 	NO_ERR; 		//return error code
		COMMAND_STRUCT CmdStruct;

		int sector = atoi(argv[1]);

		uprintf("erase sector: %d", sector);

		CmdStruct.SEraseSect.nSectorNum  		= sector;		// Sector Number to erase
		CmdStruct.SEraseSect.ulFlashStartAddr 	= 0x00;	// FlashStartAddress
		ErrorCode = m25p16_Control( CNTRL_ERASE_SECT, &CmdStruct);
	}

	if(argc == 3 && strcmp(argv[0], "read") == 0)
	{
		int start = atoi(argv[1]);
		int end = atoi(argv[2]);

    	for(int i = 0; i < end; i++)
    	{
    		unsigned short aaa;
    		ERROR_CODE Result = m25p16_Read(&aaa, i + start, 0x01);

    		if(NO_ERR == Result)
    		{
    			uprintf("%x ", aaa);
    		}
    		else
    		{
    			uprintf("error while reading\n");
    			return 1;
    		}
    	}

		return 0;

	}

	if(argc == 2 && strcmp(argv[0], "verify") == 0)
	{
		int addr, type = 0;
		int cc_error;
		unsigned short buf[128];
		int len = parse_ihex32(argv[1], buf, &addr, &type, &cc_error);

		if(type == 4)
		{
			/* high address */
			base_addr = (buf[0] << 8) | buf[1];
			base_addr <<= 16;

			uprintf("extended record, base_addr: %d\n", base_addr);
			uprintf("[ok]");

			return 0;
		}

		addr += base_addr;

		if(type == 0)
		{
		    uprintf("len: %d, off: %d, t: %d\r\n", len, addr, type);

		    int record_ok = 1;
			for(int i = 0; i < len; i++)
			{
				dsp_spi_m25p16_setup();

				unsigned short dat;
				m25p16_Read(&dat, addr + i, 0x01);

				if(dat != buf[i])
				{
					record_ok = 0;
				}
			}

			if(record_ok)
			{
				uputs("[ok]");
			}
			else
			{
				uputs("[fuck]");
			}
		}

		return 0;
	}

	if(argc == 2 && strcmp(argv[0], "write") == 0)
	{
		int addr, type = 0;
		int cc_error = 0;
		unsigned short buf[128];
		int len = parse_ihex32(argv[1], buf, &addr, &type, &cc_error);

		if(cc_error)
		{
			uprintf("cc_error");
			return 1;
		}

		if(type == 4)
		{
			/* high address */
			base_addr = (buf[0] << 8) | buf[1];
			base_addr <<= 16;

			uprintf("extended record, base_addr: %d\n", base_addr);

			return 0;
		}

		addr += base_addr;

		if(type == 0)
		{
		    uprintf("len: %d, off: %d, t: %d\r\n", len, addr, type);

			for(int i = 0; i < len; i++)
			{
				dsp_spi_m25p16_setup();
				unsigned short dat = buf[i];
				m25p16_Write(&dat, addr + i, 0x01);

				/*dsp_spi_m25p16_setup();
				unsigned short verify_dat;
				m25p16_Read(&verify_dat, addr + i, 0x01);

				if(verify_dat != dat)
				{
					int retry_count = 0;

					while(retry_count < 10)
					{
						m25p16_Write(&dat, addr + i, 0x01);
						m25p16_Read(&verify_dat, addr + i, 0x01);

						retry_count++;

						if(verify_dat == dat)
						{
							break;
						}
					}
				}*/
			}
		}

		return 0;
	}

	return 2;
}
