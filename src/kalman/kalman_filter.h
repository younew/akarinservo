/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef KALMAN_FILTER_H__
#define KALMAN_FILTER_H__

/* general vectors */
typedef struct { float x, y, z; } INSVector3f;

/* general matrix */
typedef struct
{
    float
    m11, m12, m13,
    m21, m22, m23,
    m31, m32, m33;
    
} INSMatrix33f;

#define VECTOR3F_ZERO   insVector3fMake(0.0, 0.0, 0.0)

/* Linear algebra functions
 * ------------------------
 */

/** Generate a 3x3 identity matrix  */
#define M33F_IDENTITY insMatrix33fIdentity()

/** Generate a 3x3 zero matrix */
#define M33F_ZERO     insMatrix33fZero()

/**
 *  Multiply a 3x3 matrix with a vector
 *
 *  @param m 3x3 matrix
 *  @param v vector
 *
 *  @return Multiplication result
 */
#pragma optimize_for_speed
static inline
INSVector3f insM33fxV3f(const INSMatrix33f m, const INSVector3f v)
{
    INSVector3f result = {
        .x = m.m11 * v.x + m.m12 * v.y + m.m13 * v.z,
        .y = m.m21 * v.x + m.m22 * v.y + m.m23 * v.z,
        .z = m.m31 * v.x + m.m32 * v.y + m.m33 * v.z
    };
    
    return result;
}

/**
 *  Calculate a transpose of a 3x3 matrix
 *
 *  @param a matrix
 *
 *  @return Transposed matrix
 */
#pragma optimize_for_speed
static inline
INSMatrix33f insM33fTranspose(const INSMatrix33f a)
{
    INSMatrix33f result = {
        .m11 = a.m11, .m12 = a.m21, .m13 = a.m31,
        .m21 = a.m12, .m22 = a.m22, .m23 = a.m32,
        .m31 = a.m13, .m32 = a.m23, .m33 = a.m33
    };
    
    return result;
}

/**
 *  Calculate the sum of 2 matrices
 *
 *  @param a A
 *  @param b B
 *
 *  @return Sum
 */
#pragma optimize_for_speed
static inline
INSMatrix33f insM33fPlus(const INSMatrix33f a, const INSMatrix33f b)
{
    INSMatrix33f result = {
        .m11 = a.m11 + b.m11, .m12 = a.m12 + b.m12, .m13 = a.m13 + b.m13,
        .m21 = a.m21 + b.m21, .m22 = a.m22 + b.m22, .m23 = a.m23 + b.m23,
        .m31 = a.m31 + b.m31, .m32 = a.m32 + b.m32, .m33 = a.m33 + b.m33,
    };
    
    return result;
}

/**
 *  Subtract matrix b from matrix a
 *
 *  @param a A
 *  @param b B
 *
 *  @return Difference
 */
#pragma optimize_for_speed
static inline
INSMatrix33f insM33fMinus(const INSMatrix33f a, const INSMatrix33f b)
{
    INSMatrix33f result = {
        .m11 = a.m11 - b.m11, .m12 = a.m12 - b.m12, .m13 = a.m13 - b.m13,
        .m21 = a.m21 - b.m21, .m22 = a.m22 - b.m22, .m23 = a.m23 - b.m23,
        .m31 = a.m31 - b.m31, .m32 = a.m32 - b.m32, .m33 = a.m33 - b.m33,
    };
    
    return result;
}

/**
 *  Calculate the muplication of two 3x3 matrices
 *
 *  @param a A
 *  @param b B
 *
 *  @return Mutiplication result
 */
INSMatrix33f insM33fxM33f(const INSMatrix33f a, const INSMatrix33f b);

/**
 *  Calculate the inverse of a 3x3 matrix
 *
 *  @param a Matrix
 *
 *  @return Inverse
 */
INSMatrix33f insM33fInverse(const INSMatrix33f a);

/**
 *  Generate a 3x3 identity matrix
 *
 *  @return The identity matrix
 */
#pragma optimize_for_speed
static inline
INSMatrix33f insMatrix33fIdentity(void)
{
    INSMatrix33f I = {
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0
    };
    
    return I;
}

/**
 *  Generate a zero 3x3 matrix
 *
 *  @return The zero matrix
 */
#pragma optimize_for_speed
static inline
INSMatrix33f insMatrix33fZero(void)
{
    INSMatrix33f I = {
        0.0, 0.0, 0.0,
        0.0, 0.0, 0.0,
        0.0, 0.0, 0.0
    };
    
    return I;
}

/**
 *  Calculate the sum of two vectors
 *
 *  @param a A
 *  @param b B
 *
 *  @return Sum
 */
#pragma optimize_for_speed
static inline
INSVector3f insVector3fPlus(const INSVector3f a, const INSVector3f b)
{
    INSVector3f result = {
        .x = a.x + b.x, .y = a.y + b.y, .z = a.z + b.z
    };
    
    return result;
}

/**
 *  Subtract vector b from vector a
 *
 *  @param a A
 *  @param b B
 *
 *  @return Subtraction result
 */
#pragma optimize_for_speed
static inline
INSVector3f insVector3fMinus(const INSVector3f a, const INSVector3f b)
{
    INSVector3f result = {
        .x = a.x - b.x, .y = a.y - b.y, .z = a.z - b.z
    };
    
    return result;
}

/**
 *  Multiply a by b
 *
 *  @param a A
 *  @param b B
 *
 *  @return Multiplication result
 */
#pragma optimize_for_speed
static inline
INSVector3f insVector3fMultiply(const INSVector3f a, const INSVector3f b)
{
    INSVector3f result = {
        .x = a.x * b.x, .y = a.y * b.y, .z = a.z * b.z
    };
    
    return result;
}

/* Miscellaneous functions
 * -----------------------
 */

/**
 *  Create a INSVector3f
 *
 *  @param x x
 *  @param y y
 *  @param z z
 *
 *  @return The INSVector4f value
 */
#pragma optimize_for_speed
static inline
INSVector3f insVector3fMake(const float x, const float y, const float z)
{
    INSVector3f vector = { x, y, z };
    return vector;
}

typedef struct
{
    INSMatrix33f A; /* State transition matrix */
    INSMatrix33f Q; /* Estimated process error covariance */
    INSMatrix33f R; /* Estimated measurement error covariance */
    INSMatrix33f B; /* Control matrix */
    INSMatrix33f H; /* Observation matrix */
    
    INSVector3f  previousX;
    INSMatrix33f previousP;
    
} INSKalmanFilter;

/**
 *  Initialize a Kalman filter
 *
 *  @param filter A pointer to the Kalman filter
 *  @param A      State transition matrix
 *  @param Q      Estimated process error covariance
 *  @param R      Estimated measurement error covariance
 *  @param B      Control matrix
 *  @param H      Observation matrix
 *
 *  @return A pointer to the input Kalman filter
 */
INSKalmanFilter *insKalmanFilterInit(
    INSKalmanFilter *filter,
    const INSMatrix33f A, const INSMatrix33f Q,
    const INSMatrix33f R, const INSMatrix33f B, const INSMatrix33f H
);

/**
 *  Update the Kalman filter and estimate a new measurement
 *
 *  @param filter A pointer to the Kalman filter
 *  @param z      Measurement vector
 *  @param u      Control vector
 *
 *  @return Estimated measurement vector
 */
INSVector3f insKalmanFilterUpdate(
    INSKalmanFilter *filter,
    const INSVector3f z, const INSVector3f u
);

#endif /* defined(KALMAN_FILTER_H__) */
