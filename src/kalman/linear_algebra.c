/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "kalman_filter.h"

#pragma optimize_for_speed
INSMatrix33f insM33fxM33f(const INSMatrix33f a, const INSMatrix33f b)
{
    INSMatrix33f result = {
        .m11 = a.m11 * b.m11 + a.m12 * b.m21 + a.m13 * b.m31,
        .m12 = a.m11 * b.m12 + a.m12 * b.m22 + a.m13 * b.m32,
        .m13 = a.m11 * b.m13 + a.m12 * b.m23 + a.m13 * b.m33,
        
        .m21 = a.m21 * b.m11 + a.m22 * b.m21 + a.m23 * b.m31,
        .m22 = a.m21 * b.m12 + a.m22 * b.m22 + a.m23 * b.m32,
        .m23 = a.m21 * b.m13 + a.m22 * b.m23 + a.m23 * b.m33,
        
        .m31 = a.m31 * b.m11 + a.m32 * b.m21 + a.m33 * b.m31,
        .m32 = a.m31 * b.m12 + a.m32 * b.m22 + a.m33 * b.m32,
        .m33 = a.m31 * b.m13 + a.m32 * b.m23 + a.m33 * b.m33
    };
    
    return result;
}

#pragma optimize_for_speed
INSMatrix33f insM33fInverse(const INSMatrix33f a)
{
    INSMatrix33f result = {
        .m11 = ((-a.m23)  *  a.m32 + a.m22 * a.m33) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m12 = (a.m13 * a.m32 - a.m12 * a.m33) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m13 = ((-a.m13) * a.m22 + a.m12 * a.m23) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m21 = (a.m23 * a.m31 - a.m21 * a.m33) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m22 = ((-a.m13) * a.m31 + a.m11 * a.m33) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m23 = (a.m13 * a.m21 - a.m11 * a.m23) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m31 = ((-a.m22) * a.m31 + a.m21 * a.m32) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m32 = (a.m12 * a.m31 - a.m11 * a.m32) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33),
        
        .m33 = ((-a.m12) * a.m21 + a.m11 * a.m22) /
        ((-a.m13) * a.m22 * a.m31 + a.m12 * a.m23 * a.m31 + a.m13 *
         a.m21 * a.m32 - a.m11 * a.m23 * a.m32 - a.m12 * a.m21 *
         a.m33 + a.m11 * a.m22 * a.m33)
    };
    
    return result;
}
