/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "motor_control.h"
#include <inttypes.h>
#include <stdint.h>

#include "utils/cJSON.h"
#include "command/command.h"

int command_labled = 0;

char *extract_labeled_command(char *cmd_in)
{
    char command_label[32] = { 0 };
    char *ptr = command_label;

    if(*cmd_in == '*')
    {
        while(*cmd_in++ != ' ' && *cmd_in != '\0')
        {
            *ptr++ = *cmd_in;
        }

        command_labled = atoi(command_label);
    }
    else
    {
        command_labled = 0;
    }

    return cmd_in;
}

#define COMMAND_MAX_LENGTH 1024

// TODO: fix quote
void process_command(void)
{
	if(uart_rx_idle == 0)
	{
		return;
	}

	uart_rx_idle = 0;

    /* clear state machine states */
    int rx_bytes = uart_rx_count;
    uart_rx_count = 0;

    /* drop the pack if is too long */
    if(rx_bytes >= COMMAND_MAX_LENGTH)
    {
        return;
    }

    /* copy command to a new array */
    char command[COMMAND_MAX_LENGTH];
    memcpy(command, uart_rx_buffer, rx_bytes);
    command[rx_bytes] = '\0';

    /* see if is a legal command string */
    if(rx_bytes < 2)
    {
        /* should have at least 2 bytes */
        return;
    }

    json_call_parse(command);

    /* *<addr> <cmd> <arg>... */
    /* a legal command string starts with '*' */
    if(command[0] == '*')
    {
        /* for legacy support,
         * the older version uses ';' as a termination char */
        if(command[rx_bytes - 1] == ';')
        {
            command[rx_bytes - 1] = '\0';
        }

        /* parse command label and get new pointer */
        char *command_head = extract_labeled_command(command);

        if(command_labled == mc_session.controller_configs.local_address ||
        		command_labled == -1 ||
				command_labled == -2)
        {
            if(command_labled == -1)
            {
                /* broadcast message, disable echo,
                 * set output FIFO to NULL */
                uart_out_redirect(NULL);
            }

            uprintf("\r\n[%d]=", command_labled);

            LED_RED_LOW();
            int ret = execute_command(&mc_session, command_list, command_head);
            LED_RED_HIGH();

            if(ret != 0)
            {
                if(ret == -1)
                {
                    /* command not found */
                    uputs("???");
                }
                else
                {
                    uprintf("\r\ncommand returns: %d", ret);
                }
            }

            uputs(";");

            /* restore output FIFO */
            if(command_labled == -1)
            {
                uart_out_redirect(&uart_tx_fifo);
            }
        }

    }
}
