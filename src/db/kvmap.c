/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "kvmap.h"
#include <stdio.h>

#include "../hw/sharc_hw.h"
#include "../utils/log.h"

static inline
unsigned int hash(uint32_t str, int max)
{
    return str % max;
}

KVMap *kvmap_create(int hashArraySize,
                    fram_write_word fram_write_func, fram_read_word fram_read_func)
{
#ifdef KVMAP_LOG
	lprintf("kvmap: create: hash array size %d\r\n", hashArraySize);
#endif /* KVMAP_LOG */

    KVMap *map = malloc(sizeof(KVMap));
    if(!map) goto kvmap_err_out_of_memory;
    
    map->hashArraySize = hashArraySize;
    
    map->root = malloc(sizeof(KVMapNode) * map->hashArraySize);
    if(!map) goto kvmap_err_out_of_memory;
    
    map->fram_read_func = fram_read_func;
    map->fram_write_func = fram_write_func;
    
    if(!map->root)
    {
#ifdef KVMAP_LOG
	lprintf("kvmap: cannot create root node\r\n");
#endif /* KVMAP_LOG */

        free(map);
        goto kvmap_err_out_of_memory;
    }
    
    for(int i = 0; i < map->hashArraySize; i++)
    {
        map->root[i].key = 0;
        
        map->root[i].next = malloc(sizeof(KVMapNode));
        if(!map) goto kvmap_err_out_of_memory;
        
        map->root[i].next->next = NULL;
        
        map->root[i].value = (void *)-1;
    }
    
    return map;
    
kvmap_err_out_of_memory:
#ifdef KVMAP_LOG
	lprintf("kvmap: out of memory\r\n");
#endif /* KVMAP_LOG */
    return NULL;
}

KVMapNode *kvmap_findimp(KVMap *map, uint32_t key)
{
    int index = hash(key, map->hashArraySize);
    
    /* root level and nothing stored in this node */
    if(!map->root[index].next)
        return NULL;
    
    /* stop immediately if the first entry matches the key */
    if(map->root[index].key == key)
        return &map->root[index];
    
    KVMapNode *current = map->root[index].next;
    KVMapNode *prev = &map->root[index];
    
    while(current && current->next)
    {
        prev = current;
        current = current->next;

        if(key == prev->key)
            return prev;
    }
    
    assert(current);
    assert(!current->next);
    
    return prev;
}

KVMapNode *kvmap_find(KVMap *map, uint32_t key)
{
    KVMapNode *result = kvmap_findimp(map, key);
    
    if(result && result->key == key)
        return result;
    else
        return NULL;
}

KVMap *kvmap_insert(KVMap *map, uint32_t key, void *value, int len)
{
    KVMapNode *item = kvmap_findimp(map, key);
    
    if(item)
    {
        /* test if the key is the same */
        if(key == item->key)
        {
        	//printf("key duplicate: [%d] [%d]", key, item->key);
            return NULL;
        }
        
        item = item->next;
    }
    else
    {
        item = &map->root[hash(key, map->hashArraySize)];
    }
    
    item->key = key;
    item->value = value;
    item->len = len;
    
    item->next = malloc(sizeof(KVMapNode));
    if(!map) return NULL;
    
    item->next->next = NULL;
    item->next->key = UINT32_MAX - 1;
    
    return map;
}

KVMapNode *kvmap_remove(KVMap *map, uint32_t key)
{
    KVMapNode *item = kvmap_findimp(map, key);
    
    if(item)
    {
        int index = hash(key, map->hashArraySize);
        
        KVMapNode *current = &map->root[index];
        KVMapNode *prev = current;
        
        /* root level item */
        if(item == current)
        {
            return item;
        }
        
        while(current)
        {
            prev = current;
            current = current->next;
            if(current == item)
            {
                (prev)->next = item->next;
                return item;
            }
        }
    }
    
    return NULL;
}

void kvmap_destory(KVMap *map)
{
    assert(map);
    
    for(int i = 0; i < map->hashArraySize; i++)
    {
        KVMapNode *item = map->root[i].next;
        
        while (item)
        {
            KVMapNode *next = item->next;
            
            free(item);
            
            item = next;
        }
    }
    
    free(map->root);
    free(map);
}

int kvmap_save_variables(KVMap *map)
{
#define STORE_VARIABLE()                                        \
do {                                                            \
    if(current->key)                                            \
    {                                                           \
        map->fram_write_func(current_ptr++, current->key);      \
        map->fram_write_func(current_ptr++, current->len);      \
                                                                \
        for(int j = 0; j < current->len; j++)                   \
        {                                                       \
            map->fram_write_func(current_ptr++,                 \
            ((uint32_t *)current->value)[j]);                   \
        }                                                       \
    }                                                           \
} while(0)
    
    int current_ptr = 10;
    
    for(int i = 0; i < map->hashArraySize; i++)
    {
        KVMapNode *current = &map->root[i];
        
        if(current->next)
        {
            STORE_VARIABLE();
        }
        
        while (current)
        {
            current = current->next;
            if(current)
            {
                if(current->next)
                {
                    STORE_VARIABLE();
                }
            }
        }
    }
    
    /* write length */
    map->fram_write_func(0, KVMAP_MAGIC);
    map->fram_write_func(1, current_ptr);
    
    /* reserved */
    for(int i = 2; i < 10; i++)
    {
        map->fram_write_func(i, 0);
    }

    return current_ptr;
}

KVMap *kvmap_load_variables(KVMap *map)
{
    if(map->fram_read_func(0) != KVMAP_MAGIC)
    {
        return NULL;
    }
    
    /* read record length */
    uint32_t table_len = map->fram_read_func(1);

    uint32_t offset = 10;
    while(offset < table_len)
    {
        uint32_t key = map->fram_read_func(offset++);
        uint32_t entry_len = map->fram_read_func(offset++);
        
        /* find variable in the k-v map */
        KVMapNode *node = kvmap_find(map, key);
        
        if(node && node->len == entry_len)
        {
#ifdef KVMAP_VERBOSE
            uprintf("key: %d:%d, len: %d: ",
                   (key & 0xffff0000) >> 16,
                   key & 0x0000ffff,
                   entry_len);
#endif /* KVMAP_VERBOSE */
            
            for(uint32_t j = 0; j < entry_len; j++)
            {
                /* read word from FRAM */
                uint32_t word = map->fram_read_func(offset + j);
                
                /* write word to the pointer */
                ((uint32_t *)node->value)[j] = word;
                
#ifdef KVMAP_VERBOSE
                uprintf("%x ", word);
#endif /* KVMAP_VERBOSE */
            }
#ifdef KVMAP_VERBOSE
            uprintf("\n");
#endif /* KVMAP_VERBOSE */
        }
        else
        {
        	/* key not found */
#ifdef KVMAP_LOG
        	lprintf("kvmap: key not found: %d:%d\r\n",
        			(key & 0xffff0000) >> 16,
        			 key & 0x0000ffff);
#endif /* KVMAP_LOG */
        }

        offset += entry_len;
    }
    
#ifdef KVMAP_LOG
        	lprintf("kvmap: final offset: %d\r\n", offset);
#endif /* KVMAP_LOG */

    return map;
}

int kvmap_link_variable(
    KVMap *map,
    uint32_t major, uint32_t minor,
    void *ptr, uint32_t len)
{
    uint32_t key = (major & 0xffff) << 16 | (minor & 0xffff);
    if(!kvmap_insert(map, key, ptr, len))
    {
    	printf("kvmap: link: out of memory\n");
    	return 1;
    }
    
    return 0;
}
