/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef KVMAP_H__
#define KVMAP_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stddef.h>
#include <stdint.h>

//#define KVMAP_VERBOSE
#define KVMAP_LOG
#define KVMAP_MAGIC     0x93933939

typedef int         (*fram_write_word)(int, uint32_t);
typedef uint32_t    (*fram_read_word)(int);

typedef struct KVMapNode
{
    struct KVMapNode *next;

    uint32_t key;
    uint32_t len;
    void *value;
} KVMapNode;

typedef struct
{
    KVMapNode *root;
    int hashArraySize;
    
    fram_write_word fram_write_func;
    fram_read_word  fram_read_func;
} KVMap;

/** create a KVMap */
KVMap *kvmap_create(int hashArraySize,
                    fram_write_word fram_write_func,
                    fram_read_word fram_read_func);

KVMapNode *kvmap_find(KVMap *map, uint32_t key);

/** insert an entry to the map */
KVMap *kvmap_insert(KVMap *map, uint32_t key, void *value, int len);

/** remove an entry from the map */
KVMapNode *kvmap_remove(KVMap *map, uint32_t key);

/** release the memory allocated to the map */
void kvmap_destory(KVMap *map);

/** export variables to an external storage */
int kvmap_save_variables(KVMap *map);

/** load variables from external storage */
KVMap *kvmap_load_variables(KVMap *map);

/** link a variable */
int kvmap_link_variable(
    KVMap *map,
    uint32_t major, uint32_t minor,
    void *ptr, uint32_t len
);

#endif /* defined(KVMAP_H__) */
